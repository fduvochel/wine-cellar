/* eslint-disable @typescript-eslint/no-var-requires */
const { pathsToModuleNameMapper } = require('ts-jest');
const { compilerOptions } = require('./tsconfig');

const nextJest = require('next/jest');

const createJestConfig = nextJest({
  // Provide the path to your Next.js app to load next.config.js and .env files in your test environment
  dir: '.',
});

const customJestConfig = {
  globals: {
    'ts-jest': {
      compiler: 'ttypescript',
    },
  },
  rootDir: './',
  roots: ['./src'],
  modulePaths: [compilerOptions.baseUrl],
  setupFiles: ['jest-ts-auto-mock'],
  setupFilesAfterEnv: ['./jest.setup.js'],
  moduleDirectories: ['node_modules', './src/'],
  transform: {
    '^.+\\.(ts|tsx|js|jsx)$': 'ts-jest',
  },
  moduleFileExtensions: ['js', 'ts', 'tsx'],
  moduleNameMapper: pathsToModuleNameMapper(compilerOptions.paths),
};

module.exports = createJestConfig(customJestConfig);
