# Wine Cellar

A Web Application manage your wine cellar. You can manually store bottles and their purchase information or import all the records from an excel file.

## Command

install dependencies and setup monorepo

```bash
yarn bootstrap
```

```bash
yarn dev
```

```bash
yarn build
```

```bash
yarn lint
```

## Git flow

Commit are linted and should follow [conventionnal changelog](https://github.com/conventional-changelog/conventional-changelog) standards.

Moreover the context should correspond to a packages name (web, api ...).
[commit lint config](https://github.com/conventional-changelog/commitlint/tree/master/@commitlint/config-lerna-scopes)
