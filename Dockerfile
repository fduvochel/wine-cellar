FROM node:current-bookworm

WORKDIR /app

COPY ./build .
COPY ./.env.production .
COPY package.json yarn.lock next.config.js ./

RUN yarn --production --frozen-lockfile

CMD [ "yarn", "start" ]

EXPOSE 3000