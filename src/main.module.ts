import Next from 'next';
import { RenderModule } from 'nest-next';
import winston from 'winston';
import { WinstonModule, utilities } from 'nest-winston';
import { APP_FILTER } from '@nestjs/core';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule, ConfigService } from '@nestjs/config';

import { validationSchema } from '@config/schema';
import { typeOrmConfigFactory } from '@config/typeorm';
import { BottlesModule } from '@bottles/bottles.module';
import { StockModule } from '@stock/stock.module';
import { InventoryModule } from '@inventory/inventory.module';
import { VoucherModule } from '@voucher/voucher.module';
import { ErrorFilter } from '@error.filter';
import { AuthModule } from '@auth/auth.module';
import { UsersModule } from '@users/users.module';
import { AppModule } from 'app/app.module';

const envFilePath = [`.env.${process.env.NODE_ENV}`];

@Module({
  imports: [
    BottlesModule,
    StockModule,
    InventoryModule,
    VoucherModule,
    AuthModule,
    UsersModule,
    AppModule,
    RenderModule.forRootAsync(
      Next({
        dev: process.env.NODE_ENV === 'development',
      }),
    ),
    WinstonModule.forRoot({
      level: 'info',
      transports: [
        new winston.transports.Console({
          silent: false,
          format: winston.format.combine(
            winston.format.timestamp(),
            utilities.format.nestLike(),
          ),
        }),
      ],
    }),
    ConfigModule.forRoot({
      isGlobal: true,
      validationSchema,
      envFilePath,
    }),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: typeOrmConfigFactory,
    }),
  ],
  controllers: [],
  providers: [{ provide: APP_FILTER, useClass: ErrorFilter }],
})
export class MainModule {}
