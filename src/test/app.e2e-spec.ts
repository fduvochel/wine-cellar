import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import request from 'supertest';
import { Repository } from 'typeorm';

import { MainModule } from '../main.module';
import bottleFixtures from '@model/fixtures/bottle-fixtures.json';
import voucherFixture from '@model/fixtures/voucher-fixtures.json';
import { Bottle } from '@model/entities/bottle.entity';
import { Stock } from '@model/entities/stock.entity';
import { StockDTO } from '@model/dto/stock.dto';
import { Dining } from '@model/entities/dining.entity';
import { Dish } from '@model/entities/dish.entity';
import { Inventory } from '@model/entities/inventory.entity';
import { BottleRequestDTO } from '@model/dto/bottleRequest.dto';
import { JwtModule, JwtService } from '@nestjs/jwt';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { EnvironmentVariables } from '@model/enums/environment.enum';
import { UserDTO } from '@model/dto/user.dto';
import { User } from '@model/entities/user.entity';
import userFixture from '@model/fixtures/user-fixture.json';
import { Credentials } from '@model/entities/credentials.entity';

describe('AppController (e2e)', () => {
  let app: INestApplication;
  let token: string;
  let userDTO: UserDTO;
  let user: User;
  let userRepository: Repository<User>;
  let inventory: Inventory;
  let inventoryRepository: Repository<Inventory>;
  let jwtService: JwtService;
  let credentialsRepository: Repository<Credentials>;

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [
        MainModule,
        JwtModule.registerAsync({
          imports: [ConfigModule],
          useFactory: async (configService: ConfigService) => ({
            secret: configService.get<string>(EnvironmentVariables.SECRET),
          }),
          inject: [ConfigService],
        }),
      ],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();

    userRepository = app.get('UserRepository');
    inventoryRepository = app.get('InventoryRepository');
    jwtService = app.get('JwtService');
    credentialsRepository = app.get('CredentialsRepository');

    user = await userRepository.save(userFixture[0]);
    inventory = await inventoryRepository.save(new Inventory());
    user.inventory = inventory;
    await userRepository.save(user);

    userDTO = UserDTO.fromEntity(user);
    token = await jwtService.signAsync(
      {
        data: userDTO,
      },
      { expiresIn: '1d' },
    );
  });

  describe('Bottles module', () => {
    let bottleRepository: Repository<Bottle>;
    let bottleRequestDTO: BottleRequestDTO;
    let stockDTO: StockDTO;

    beforeAll(() => {
      bottleRepository = app.get('BottleRepository');
    });

    beforeAll(async () => {
      const bottles = bottleFixtures.map(bottle => {
        bottle.stocks.map(stock => {
          stock.inventory = inventory;
          return stock;
        });
        return bottle;
      });
      await Promise.all(bottles.map(bottle => bottleRepository.save(bottle)));
    });

    describe('when GET /bottles', () => {
      it('should return an array of BottleResponseDTO', async () => {
        const res = await request(app.getHttpServer())
          .get('/bottles')
          .set('Authorization', 'Bearer ' + token)
          .expect(200);
        expect(res.body).toHaveLength(2);
      });

      describe('when GET /bottles with no Bottle in base', () => {
        beforeAll(async () => {
          await bottleRepository.query('TRUNCATE TABLE bottle CASCADE');
        });

        it('should return an empty array ', async () => {
          return await request(app.getHttpServer())
            .get('/bottles')
            .set('Authorization', 'Bearer ' + token)
            .expect(200)
            .expect([]);
        });
        afterAll(async () => {
          await Promise.all(
            bottleFixtures.map(bottle => bottleRepository.save(bottle)),
          );
        });
      });
    });

    describe('when POST /bottles', () => {
      bottleRequestDTO = new BottleRequestDTO();
      bottleRequestDTO.domain = 'test';
      stockDTO = new StockDTO();
      stockDTO.quantity = 1;
      bottleRequestDTO.stock = stockDTO;

      it('should return a BottleResponseDTO with stocks', async () => {
        const res = await request(app.getHttpServer())
          .post('/bottles')
          .set('Authorization', 'Bearer ' + token)
          .set('Accept', 'application/json')
          .send(bottleRequestDTO)
          .expect(201);
        expect(res.body.id).toBeDefined();
        expect(res.body.domain).toBe('test');
        expect(res.body.stocks[0].id).toBeDefined();
      });
    });

    afterAll(async () => {
      await bottleRepository.query('TRUNCATE TABLE bottle CASCADE;');
    });
  });

  describe('Stock module', () => {
    let bottleRepository: Repository<Bottle>;
    let stockRepository: Repository<Stock>;
    let stocks: StockDTO[];

    beforeAll(() => {
      bottleRepository = app.get('BottleRepository');
      stockRepository = app.get('StockRepository');
    });

    beforeAll(async () => {
      await Promise.all(
        bottleFixtures.map(bottle => bottleRepository.save(bottle)),
      );
    });

    describe('when GET /stocks', () => {
      beforeAll(async () => {
        stocks = (await stockRepository.find()).map(stock =>
          StockDTO.fromEntity(stock),
        );
      });

      it('should return an array of StockDTO', async () => {
        const res = await request(app.getHttpServer())
          .get('/stocks')
          .set('Authorization', 'Bearer ' + token)
          .expect(200);
        expect(res.body).toHaveLength(3);
        expect(res.body[0].id).toEqual(stocks[0].id);
        expect(res.body[1].id).toEqual(stocks[1].id);
        expect(res.body[2].id).toEqual(stocks[2].id);
      });
    });

    afterAll(async () => {
      await bottleRepository.query('TRUNCATE TABLE bottle CASCADE;');
    });
  });

  describe('Dining module', () => {
    let diningRepository: Repository<Dining>;
    let bottleRepository: Repository<Bottle>;
    let dishRepository: Repository<Dish>;
    let bottles: Bottle[];
    let voucher: any;

    beforeAll(() => {
      diningRepository = app.get('DiningRepository');
      bottleRepository = app.get('BottleRepository');
      dishRepository = app.get('DishRepository');
    });

    beforeAll(async () => {
      bottles = await Promise.all(
        bottleFixtures.map(bottle => bottleRepository.save(bottle)),
      );
      voucher = voucherFixture[0];
      voucher.stockId = bottles[0].stocks[0].id;
    });

    describe('when POST /dining', () => {
      it('should save Dining and Dish and return a VoucherDTO', async () => {
        const res = await request(app.getHttpServer())
          .post('/dining')
          .set('Accept', 'application/json')
          .set('Authorization', 'Bearer ' + token)
          .send(voucher)
          .expect(201);
        expect(res.body.id).toBeDefined();
        const dinings = await diningRepository.find();
        expect(dinings).toHaveLength(1);
        const dishes = await dishRepository.find();
        expect(dishes).toHaveLength(1);
      });
    });

    afterAll(async () => {
      try {
        await bottleRepository.query('TRUNCATE TABLE bottle CASCADE;');
        await diningRepository.query('TRUNCATE TABLE dining CASCADE;');
        await dishRepository.query('TRUNCATE TABLE dish CASCADE;');
      } catch (err) {
        throw new Error(err);
      }
    });
  });

  describe('Inventory module', () => {
    let inventoryRepository: Repository<Inventory>;
    let inventory: Inventory;

    beforeAll(() => {
      inventoryRepository = app.get('InventoryRepository');
    });

    describe('when GET /inventory', () => {
      beforeAll(async () => {
        inventory = await inventoryRepository.save(new Inventory());
      });

      it('should return an array of Inventory', async () => {
        const res = await request(app.getHttpServer())
          .get('/inventory')
          .expect(200);
        expect(res.body).toHaveLength(2);
        expect(res.body[1].id).toBe(inventory.id);
      });
    });

    afterAll(async () => {
      try {
        await inventoryRepository.query('TRUNCATE TABLE inventory CASCADE;');
      } catch (err) {
        throw new Error(err);
      }
    });
  });

  afterAll(async () => {
    try {
      await credentialsRepository.query('TRUNCATE TABLE credentials CASCADE;');
      await inventoryRepository.query('TRUNCATE TABLE inventory CASCADE;');
    } catch (err) {
      throw new Error(err);
    }
    await app.close();
  });
});
