import { IBottleResponse } from '@shared';

export const bottleMock: IBottleResponse = {
  id: 'testId',
  domain: 'Test Domain',
  color: 'red',
  region: 'test Region',
  appellation: 'test appellation',
  vintage: 2010,
  label: 'test label',
  readyYear: 2010,
  plateauStartYear: 2018,
  plateauEndYear: 2025,
  expirationYear: 2035,
  expirationString: 'long time',
  comment: 'excellent with cabbage',
  photos: [],
  quantityInStock: 3,
  stocks: [],
};
