export const stockMock = {
  id: 'stockId',
  bottleId: 'bottleId',
  quantity: 3,
  purchaseDate: new Date(Date.now()),
  createDateTime: new Date(Date.now()),
  position: 'stock position',
  owner: 'stock owner',
  supplier: 'stock supplier',
  size: 'stock size',
  price: 10,
  remainQuantity: 2,
};
