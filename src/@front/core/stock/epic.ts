import { combineEpics, Epic, ofType } from 'redux-observable';
import { of } from 'rxjs';
import { catchError, switchMap, map } from 'rxjs/operators';
import { IAppDependencies, IAppState } from '..';
import { AppAction } from '../_types/action';
import {
  fetchStockByIdSuccess,
  fetchStockIdError,
  StockActions,
} from './action';

export type StockEpic = Epic<
  AppAction<StockActions>,
  AppAction<StockActions>,
  IAppState,
  IAppDependencies
>;

export const findOneStockEpic: StockEpic = (action$, state, { stockApi }) =>
  action$.pipe(
    ofType(StockActions.FETCH_STOCK_ID_PENDING),
    switchMap(({ payload }) => stockApi.getStockById(payload)),
    map(fetchStockByIdSuccess),
    catchError(err => of(fetchStockIdError(err))),
  );

export default combineEpics(findOneStockEpic);
