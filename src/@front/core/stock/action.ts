import { IStock } from '@shared';
import { AppActionCreator } from '../_types/action';

export enum StockActions {
  FETCH_STOCK_ID_PENDING = 'stocks/getStockByIdPending',
  FETCH_STOCK_ID_SUCCESS = 'stocks/getStockByIdSuccess',
  FETCH_STOCK_ID_ERROR = 'stocks/getStockByIdError',
  CACHE_STOCK = 'stocks/cacheStock',
  RESET_STOCKS_STATE = 'stocks/resetStocksState',
}

export const fetchStockById: AppActionCreator<StockActions, string> = (
  payload: string,
) => ({
  type: StockActions.FETCH_STOCK_ID_PENDING,
  payload,
});

export const fetchStockByIdSuccess: AppActionCreator<StockActions, any> = (
  payload: any,
) => ({
  type: StockActions.FETCH_STOCK_ID_SUCCESS,
  payload,
});

export const fetchStockIdError: AppActionCreator<StockActions, Error> = (
  payload: Error,
) => ({
  type: StockActions.FETCH_STOCK_ID_ERROR,
  payload,
});

export const cacheStockInState: AppActionCreator<StockActions, IStock> = (
  payload: IStock,
) => ({
  type: StockActions.CACHE_STOCK,
  payload,
});

export const resetStocksState: AppActionCreator<StockActions> = () => ({
  type: StockActions.RESET_STOCKS_STATE,
});
