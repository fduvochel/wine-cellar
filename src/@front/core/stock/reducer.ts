import { IStock } from '@shared';
import { Reducer } from 'redux';
import { Status } from '../_enum/Status';
import { AppAction } from '../_types/action';
import { StockActions } from './action';

export interface IStockState {
  status: string;
  list: Record<string, IStock>;
}

export const initialState: IStockState = {
  status: Status.IDLE,
  list: {},
};

export const stocksReducer: Reducer<IStockState, AppAction<StockActions>> = (
  state = initialState,
  action,
) => {
  switch (action.type) {
    case StockActions.FETCH_STOCK_ID_PENDING:
      return {
        ...state,
        status: Status.PENDING,
      };
    case StockActions.FETCH_STOCK_ID_SUCCESS:
      return {
        ...state,
        status: Status.COMPLETED,
        list: {
          ...state.list,
          [action.payload.id]: action.payload,
        },
      };
    case StockActions.CACHE_STOCK:
      return {
        ...state,
        list: {
          ...state.list,
          [action.payload.id]: action.payload,
        },
      };
    case StockActions.RESET_STOCKS_STATE:
      return initialState;
    default:
      return state;
  }
};
