import { IVoucherResponse } from '@shared';
import { Errors } from '../_enum/Errors';

class VoucherApi {
  private path = 'voucher';

  public async createVoucher(voucher: IVoucherResponse) {
    const token = localStorage.getItem('TOKEN');
    const res = await fetch(`/${this.path}`, {
      method: 'post',
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + token,
      },
      body: JSON.stringify(voucher),
    });
    if (res.status !== 201) {
      throw new Error(Errors.GENERIC);
    }
    const data = await res.json();
    return data;
  }

  public async getAll(): Promise<IVoucherResponse[]> {
    const token = localStorage.getItem('TOKEN');
    const res = await fetch(`/${this.path}`, {
      headers: {
        Authorization: 'Bearer ' + token,
      },
    });
    if (res.status !== 200) {
      throw new Error(Errors.GENERIC);
    }
    return res.json();
  }
}

export default VoucherApi;
