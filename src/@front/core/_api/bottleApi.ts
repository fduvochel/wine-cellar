import { IBottleResponse } from '@shared';
import { Errors } from '../_enum/Errors';

class BottleApi {
  private path = 'bottles';
  private date = new Date().toDateString().replace(/\s/g, '_');
  private filePath = `/files/bottles_${this.date}.xlsx`;

  async getAll() {
    const token = localStorage.getItem('TOKEN');
    const res = await fetch(`/${this.path}`, {
      headers: {
        Authorization: 'Bearer ' + token,
      },
    });
    const data = await res.json();
    return data;
  }

  async create(bottle: any) {
    const token = localStorage.getItem('TOKEN');
    const res = await fetch(`/${this.path}`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + token,
      },
      body: JSON.stringify(bottle),
    });

    const data = await res.json();
    return data;
  }

  async getById(id: string) {
    const token = localStorage.getItem('TOKEN');
    const res = await fetch(`/${this.path}/` + id, {
      headers: {
        Authorization: 'Bearer ' + token,
      },
    });
    const bottle: IBottleResponse = await res.json();
    bottle.stocks = bottle.stocks.map(stock => {
      stock.purchaseDate = new Date(stock.purchaseDate);
      return stock;
    });
    return bottle;
  }

  async uploadFile(file: File) {
    const formData = new FormData();
    formData.append('file', file);
    const token = localStorage.getItem('TOKEN');

    const res = await fetch(`/${this.path}/upload/excel`, {
      method: 'POST',
      headers: {
        Authorization: 'Bearer ' + token,
      },
      body: formData,
    });

    if (res.status !== 201) {
      throw new Error(Errors.UPLOAD);
    }

    const data = await res.json();
    return data;
  }

  async downloadFile() {
    const token = localStorage.getItem('TOKEN');
    const res = await fetch(`/${this.path}/download/excel`, {
      headers: {
        Authorization: 'Bearer ' + token,
        responseType: 'blob',
      },
    });

    if (res.status !== 200) {
      throw new Error(Errors.DOWNLOAD);
    }
    const blob = await res.blob();

    const url = window.URL.createObjectURL(
      new Blob([blob], {
        type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
      }),
    );
    const link = document.createElement('a');
    link.href = url;
    link.setAttribute('download', this.filePath);
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  }
}

export default BottleApi;
