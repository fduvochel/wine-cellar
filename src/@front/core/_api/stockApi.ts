import { IStock } from '@shared';
import { Errors } from '../_enum/Errors';

export class StockApi {
  private path = 'stocks';

  async getStockById(id: string) {
    const token = localStorage.getItem('TOKEN');
    const res = await fetch(`/${this.path}/` + id, {
      headers: {
        Authorization: 'Bearer ' + token,
      },
    });
    if (res.status !== 200) {
      throw new Error(Errors.GENERIC);
    }
    const stock: IStock = await res.json();
    return stock;
  }

  async updateStock(stock: IStock): Promise<IStock> {
    const token = localStorage.getItem('TOKEN');
    const res = await fetch(`/${this.path}/update`, {
      method: 'post',
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + token,
      },
      body: JSON.stringify(stock),
    });
    if (res.status !== 201) {
      throw new Error(Errors.GENERIC);
    }
    const result = await res.json();
    return result;
  }
}
