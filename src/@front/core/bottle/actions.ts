import { IBottleRequest, IBottleResponse } from '@shared';
import { AppActionCreator } from '../_types/action';

export enum BottleActions {
  FETCH_PENDING = 'FETCH_BOTTLES_PENDING',
  FETCH_SUCCESS = 'FETCH_BOTTLES_SUCCESS',
  FETCH_ERROR = 'FETCH_BOTTLES_ERROR',
  UPLOAD_PENDING = 'bottles/uploadPending',
  UPLOAD_SUCCESS = 'bottles/uploadSuccess',
  UPLOAD_ERROR = 'bottles/uploadError',
  DOWNLOAD_PENDING = 'bottles/downloadPending',
  DOWNLOAD_SUCCESS = 'bottles/downloadSuccess',
  DOWNLOAD_ERROR = 'bottles/downloadError',
  POST_PENDING = 'POST_BOTTLE_PENDING',
  POST_SUCCESS = 'POST_BOTTLE_SUCCESS',
  POST_ERROR = 'POST_BOTTLE_ERROR',
  FETCH_ID_PENDING = 'FETCH_ID_PENDING',
  FETCH_ID_SUCCESS = 'FETCH_ID_SUCCESS',
  FETCH_ID_ERROR = 'FETCH_ID_ERROR',
  SEARCH_BOTTLE = 'SEARCH_BOTTLE',
  RESET_SEARCH = 'bottles/resetSearch',
  RESET_BOTTLES_STATE = 'bottles/resetState',
  SORT_BOTTLES = 'bottles/sortBottles',
}

export const fetchBottles: AppActionCreator<BottleActions> = () => ({
  type: BottleActions.FETCH_PENDING,
});

export const fetchBottlesSuccess: AppActionCreator<BottleActions, any[]> = (
  payload: any[],
) => ({
  type: BottleActions.FETCH_SUCCESS,
  payload,
});

export const fetchBottlesError: AppActionCreator<BottleActions, Error> = (
  payload: Error,
) => ({
  type: BottleActions.FETCH_ERROR,
  payload,
});

export const uploadBottles: AppActionCreator<BottleActions, File> = (
  payload: File,
) => ({
  type: BottleActions.UPLOAD_PENDING,
  payload,
});

export const uploadBottlesSuccess: AppActionCreator<
  BottleActions,
  IBottleResponse[]
> = (payload: IBottleResponse[]) => ({
  type: BottleActions.UPLOAD_SUCCESS,
  payload,
});

export const uploadBottlesError: AppActionCreator<BottleActions, Error> = (
  payload: Error,
) => ({
  type: BottleActions.UPLOAD_ERROR,
  payload,
});

export const downloadBottles: AppActionCreator<BottleActions> = () => ({
  type: BottleActions.DOWNLOAD_PENDING,
});

export const downloadBottlesSuccess: AppActionCreator<BottleActions, File> = (
  payload: File,
) => ({
  type: BottleActions.DOWNLOAD_SUCCESS,
  payload,
});

export const downloadBottlesError: AppActionCreator<BottleActions, Error> = (
  payload: Error,
) => ({
  type: BottleActions.DOWNLOAD_ERROR,
  payload,
});

export const postBottle: AppActionCreator<BottleActions, IBottleRequest> = (
  payload: IBottleRequest,
) => ({
  type: BottleActions.POST_PENDING,
  payload,
});

export const postBottleSuccess: AppActionCreator<BottleActions, any> = (
  payload: any,
) => ({
  type: BottleActions.POST_SUCCESS,
  payload,
});

export const postBottleError: AppActionCreator<BottleActions, Error> = (
  payload: Error,
) => ({
  type: BottleActions.POST_ERROR,
  payload,
});

export const fetchBottlesById: AppActionCreator<BottleActions, string> = (
  payload: string,
) => ({
  type: BottleActions.FETCH_ID_PENDING,
  payload,
});

export const fetchBottlesByIdSuccess: AppActionCreator<BottleActions, any> = (
  payload: any,
) => ({
  type: BottleActions.FETCH_ID_SUCCESS,
  payload,
});

export const fetchBottlesByIdError: AppActionCreator<BottleActions, Error> = (
  payload: Error,
) => ({
  type: BottleActions.FETCH_ID_ERROR,
  payload,
});

export const searchBottle: AppActionCreator<BottleActions, string> = (
  payload: string,
) => ({
  type: BottleActions.SEARCH_BOTTLE,
  payload,
});

export const resetSearch: AppActionCreator<BottleActions> = () => ({
  type: BottleActions.RESET_SEARCH,
});

export const resetBottlesState: AppActionCreator<BottleActions> = () => ({
  type: BottleActions.RESET_BOTTLES_STATE,
});

export const sortBottles: AppActionCreator<BottleActions, IBottleResponse[]> = (
  payload: IBottleResponse[],
) => ({
  type: BottleActions.SORT_BOTTLES,
  payload,
});
