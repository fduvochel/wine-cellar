import { Reducer } from 'redux';

import { AppAction } from '../_types/action';
import { BottleActions } from './actions';
import { IBottleResponse } from '@shared';
import { Status } from '../_enum/Status';

export interface IBottleState {
  status: string;
  list: Record<string, IBottleResponse>;
  search?: string;
  error: string | null;
}

export const initialState: IBottleState = {
  status: Status.IDLE,
  list: {},
  error: null,
};

export const bottleReducer: Reducer<IBottleState, AppAction<BottleActions>> = (
  state = initialState,
  action,
) => {
  switch (action.type) {
    case BottleActions.POST_PENDING:
      return {
        ...state,
        status: Status.PENDING,
      };
    case BottleActions.POST_SUCCESS:
      return {
        ...state,
        status: Status.COMPLETED,
        list: {
          ...state.list,
          [action.payload.id]: action.payload,
        },
      };
    case BottleActions.FETCH_PENDING:
      return {
        ...state,
        status: Status.PENDING,
      };
    case BottleActions.FETCH_SUCCESS:
      return {
        ...state,
        status: Status.COMPLETED,
        list: action.payload.reduce(
          (acc: any, bottle: IBottleResponse) => ({
            ...acc,
            [bottle.id]: bottle,
          }),
          {},
        ),
      };
    case BottleActions.FETCH_ERROR:
      return {
        ...state,
        status: Status.FAILED,
        list: {},
        search: '',
        error: action.payload.message,
      };
    case BottleActions.UPLOAD_PENDING:
      return {
        ...state,
        status: Status.PENDING,
      };
    case BottleActions.UPLOAD_SUCCESS:
      return {
        ...state,
        status: Status.COMPLETED,
        list: {
          ...state.list,
          ...action.payload.reduce(
            (acc: any, bottle: IBottleResponse) => ({
              ...acc,
              [bottle.id]: bottle,
            }),
            {},
          ),
        },
      };
    case BottleActions.UPLOAD_ERROR:
      return {
        ...state,
        status: Status.FAILED,
        error: action.payload.message,
      };
    case BottleActions.DOWNLOAD_PENDING:
      return {
        ...state,
        status: Status.PENDING,
      };
    case BottleActions.DOWNLOAD_SUCCESS:
      return {
        ...state,
        status: Status.COMPLETED,
        error: null,
      };
    case BottleActions.DOWNLOAD_ERROR:
      return {
        ...state,
        status: Status.FAILED,
        error: action.payload.message,
      };
    case BottleActions.FETCH_ID_PENDING:
      return {
        ...state,
        status: Status.PENDING,
      };
    case BottleActions.FETCH_ID_SUCCESS:
      return {
        ...state,
        status: Status.COMPLETED,
        list: {
          ...state.list,
          [action.payload.id]: action.payload,
        },
      };
    case BottleActions.SEARCH_BOTTLE:
      return {
        ...state,
        search: action.payload,
      };
    case BottleActions.RESET_SEARCH:
      return {
        ...state,
        search: '',
      };
    case BottleActions.RESET_BOTTLES_STATE:
      return initialState;
    case BottleActions.SORT_BOTTLES:
      return {
        ...state,
        list: action.payload.reduce(
          (acc: any, bottle: IBottleResponse) => ({
            ...acc,
            [bottle.id]: bottle,
          }),
          {},
        ),
      };
    default:
      return state;
  }
};
