import { ofType, Epic, combineEpics } from 'redux-observable';
import { switchMap, map, catchError, filter } from 'rxjs/operators';
import { of } from 'rxjs';

import {
  BottleActions,
  fetchBottlesError,
  fetchBottlesSuccess,
  postBottleSuccess,
  postBottleError,
  fetchBottlesByIdSuccess,
  fetchBottlesByIdError,
  uploadBottlesSuccess,
  uploadBottlesError,
  downloadBottlesSuccess,
  downloadBottlesError,
} from './actions';
import { AppAction } from '../_types/action';
import { IAppDependencies, IAppState } from '..';

export type BottleEpic = Epic<
  AppAction<BottleActions>,
  AppAction<BottleActions>,
  IAppState,
  IAppDependencies
>;

export const bottleListEpic: BottleEpic = (action$, state, { bottleApi }) =>
  action$.pipe(
    ofType(BottleActions.FETCH_PENDING),
    switchMap(() => bottleApi.getAll()),
    map(fetchBottlesSuccess),
    catchError(err => of(fetchBottlesError(err))),
  );

export const uploadBottlesEpic: BottleEpic = (action$, state, { bottleApi }) =>
  action$.pipe(
    ofType(BottleActions.UPLOAD_PENDING),
    switchMap(({ payload }) => bottleApi.uploadFile(payload)),
    map(uploadBottlesSuccess),
    catchError(err => of(uploadBottlesError(err))),
  );

export const downloadBottlesEpic: BottleEpic = (
  action$,
  state,
  { bottleApi },
) =>
  action$.pipe(
    ofType(BottleActions.DOWNLOAD_PENDING),
    switchMap(() => bottleApi.downloadFile()),
    map(downloadBottlesSuccess),
    catchError(err => of(downloadBottlesError(err))),
  );

export const bottleCreateEpic: BottleEpic = (action$, state, { bottleApi }) =>
  action$.pipe(
    ofType(BottleActions.POST_PENDING),
    switchMap(({ payload }) => bottleApi.create(payload)),
    map(postBottleSuccess),
    catchError(err => of(postBottleError(err))),
  );

export const bottleInfoEpic: BottleEpic = (action$, state, { bottleApi }) =>
  action$.pipe(
    ofType(BottleActions.FETCH_ID_PENDING),
    filter(({ payload }) => {
      return !state.value.bottles.list[payload];
    }),
    switchMap(({ payload }) => bottleApi.getById(payload)),
    map(fetchBottlesByIdSuccess),
    catchError(err => of(fetchBottlesByIdError(err))),
  );

export default combineEpics(
  bottleListEpic,
  bottleCreateEpic,
  bottleInfoEpic,
  uploadBottlesEpic,
  downloadBottlesEpic,
);
