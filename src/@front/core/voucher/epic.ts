import { combineEpics, Epic, ofType } from 'redux-observable';
import { of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';

import { IAppDependencies, IAppState } from '..';
import { AppAction } from '../_types/action';
import {
  VoucherActions,
  postVoucherSuccess,
  postVoucherError,
  fetchVouchersSuccess,
  fetchVouchersError,
} from './action';

export type VoucherEpic = Epic<
  AppAction<VoucherActions>,
  AppAction<VoucherActions>,
  IAppState,
  IAppDependencies
>;

export const voucherListEpic: VoucherEpic = (action$, state, { voucherApi }) =>
  action$.pipe(
    ofType(VoucherActions.FETCH_VOUCHERS_PENDING),
    switchMap(() => voucherApi.getAll()),
    map(fetchVouchersSuccess),
    catchError(err => of(fetchVouchersError(err))),
  );

export const createVoucherEpic: VoucherEpic = (
  action$,
  state,
  { voucherApi },
) =>
  action$.pipe(
    ofType(VoucherActions.POST_VOUCHER_PENDING),
    switchMap(({ payload }) => voucherApi.createVoucher(payload)),
    map(postVoucherSuccess),
    catchError(err => of(postVoucherError(err))),
  );

export default combineEpics(voucherListEpic, createVoucherEpic);
