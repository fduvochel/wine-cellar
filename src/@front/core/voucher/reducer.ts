import { IVoucherResponse } from '@shared';
import { Reducer } from 'redux';
import { AppAction } from '../_types/action';
import { VoucherActions } from './action';
import { Status } from '../_enum/Status';

export interface IVoucherState {
  status: string;
  error: string | null;
  voucher: IVoucherResponse;
  list: Record<string, IVoucherResponse>;
}

export const initialState: IVoucherState = {
  status: Status.IDLE,
  error: null,
  voucher: {} as IVoucherResponse,
  list: {},
};

export const voucherReducer: Reducer<
  IVoucherState,
  AppAction<VoucherActions>
> = (state = initialState, action) => {
  switch (action.type) {
    case VoucherActions.FETCH_VOUCHERS_PENDING:
      return {
        ...state,
        status: Status.PENDING,
      };
    case VoucherActions.FETCH_VOUCHERS_SUCCESS:
      return {
        ...state,
        list: action.payload.reduce(
          (acc: any, voucher: IVoucherResponse) => ({
            ...acc,
            [voucher.id || '']: voucher,
          }),
          {},
        ),
        status: Status.COMPLETED,
      };
    case VoucherActions.FETCH_VOUCHERS_ERROR:
      return {
        ...state,
        list: {},
        status: Status.FAILED,
        error: action.payload.message,
      };
    case VoucherActions.POST_VOUCHER_PENDING:
      return {
        ...state,
        status: Status.PENDING,
      };
    case VoucherActions.POST_VOUCHER_SUCCESS:
      return {
        ...state,
        status: Status.COMPLETED,
        voucher: {} as IVoucherResponse,
      };
    case VoucherActions.POST_VOUCHER_ERROR:
      return {
        ...state,
        status: Status.FAILED,
        error: action.payload.message,
        voucher: {} as IVoucherResponse,
      };
    case VoucherActions.RESET_DINING_STATE:
      return initialState;
    default:
      return state;
  }
};
