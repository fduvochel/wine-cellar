import { AppActionCreator } from '../_types/action';
import { IVoucherResponse } from '@shared';

export enum VoucherActions {
  FETCH_VOUCHERS_PENDING = 'dinings/fetchVouchersPending',
  FETCH_VOUCHERS_SUCCESS = 'dinings/fetchVouchersSuccess',
  FETCH_VOUCHERS_ERROR = 'dinings/fetchVouchersError',
  POST_VOUCHER_PENDING = 'dinings/postVoucherPending',
  POST_VOUCHER_SUCCESS = 'dinings/postVoucherSuccess',
  POST_VOUCHER_ERROR = 'dinings/postVoucherError',
  RESET_DINING_STATE = 'dining/resetDiningState',
}

export const fetchVouchers: AppActionCreator<VoucherActions> = () => ({
  type: VoucherActions.FETCH_VOUCHERS_PENDING,
});

export const fetchVouchersSuccess: AppActionCreator<
  VoucherActions,
  IVoucherResponse[]
> = (payload: IVoucherResponse[]) => ({
  type: VoucherActions.FETCH_VOUCHERS_SUCCESS,
  payload,
});

export const fetchVouchersError: AppActionCreator<VoucherActions, Error> = (
  payload: Error,
) => ({
  type: VoucherActions.FETCH_VOUCHERS_ERROR,
  payload,
});

export const postVoucher: AppActionCreator<VoucherActions, IVoucherResponse> = (
  payload: IVoucherResponse,
) => ({
  type: VoucherActions.POST_VOUCHER_PENDING,
  payload,
});

export const postVoucherSuccess: AppActionCreator<
  VoucherActions,
  IVoucherResponse
> = (payload: IVoucherResponse) => ({
  type: VoucherActions.POST_VOUCHER_SUCCESS,
  payload,
});

export const postVoucherError: AppActionCreator<VoucherActions, Error> = (
  payload: Error,
) => ({
  type: VoucherActions.POST_VOUCHER_ERROR,
  payload,
});

export const resetDiningState: AppActionCreator<VoucherActions> = () => ({
  type: VoucherActions.RESET_DINING_STATE,
});
