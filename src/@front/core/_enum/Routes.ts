export enum Routes {
  WELCOME = '/welcome',
  HOME = '/',
  ADD_BOTTLE = '/add',
  DRINK_BOTTLE = '/drink/:id',
  BOTTLE_INFO = '/bottle/:id',
  ACCOUNT = '/account',
  HISTORY = '/history',
}
