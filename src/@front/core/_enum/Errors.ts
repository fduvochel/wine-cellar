export enum Errors {
  CREDENTIALS = 'Login ou mot de passe invalide',
  TOKEN = 'Invalid token',
  CREATE_USER = 'Create user error',
  UPLOAD = 'File upload error',
  DOWNLOAD = 'File download error',
  GENERIC = 'Internal server error',
}
