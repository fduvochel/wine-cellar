import { createTheme } from '@mui/material/styles';

const theme = createTheme({
  palette: {
    primary: {
      main: '#a31e55',
    },
    secondary: {
      main: '#f7c744',
    },
    text: {
      primary: '#141413',
      secondary: '#3d3c3c',
    },
    background: {
      paper: '#F5E4E4',
      default: '#F5E4E4',
    },
  },
});

export default theme;
