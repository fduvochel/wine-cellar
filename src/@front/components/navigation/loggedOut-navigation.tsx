import { Routes } from '@front/core';
import Box from '@mui/material/Box';
import React from 'react';
import AuthModal from '../auth';
import ReusableNavigation from './reusable-navigation';

const loggedOutNavigationLinks = [
  { label: 'A propos', to: Routes.WELCOME },
  { label: 'Contact', to: Routes.WELCOME },
];

const LoggedOutNavigation: React.FC = () => {
  return (
    <>
      <Box
        sx={{
          flexGrow: 1,
          m: 'auto',
          display: { xs: 'none', md: 'flex' },
          alignItems: 'center',
          justifyContent: 'center',
        }}
      >
        <ReusableNavigation links={loggedOutNavigationLinks} />
      </Box>
      <Box sx={{ flexGrow: 0 }}>
        <AuthModal />
      </Box>
    </>
  );
};

export default LoggedOutNavigation;
