import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';

import IconButton from '@mui/material/IconButton';
import MenuItem from '@mui/material/MenuItem';
import Typography from '@mui/material/Typography';
import Menu from '@mui/material/Menu';
import MenuIcon from '@mui/icons-material/Menu';

import { Routes } from '@front/core';

const loggedInPages = ['ajouter', 'historique'];
const loggedOutPages = ['A propos', 'Contact'];

interface BurgerMenuProps {
  isAuthed: boolean;
}

const BurgerMenu: React.FC<BurgerMenuProps> = ({ isAuthed }) => {
  const history = useHistory();
  const [anchorElNav, setAnchorElNav] = useState<null | HTMLElement>(null);

  const pages = isAuthed ? loggedInPages : loggedOutPages;

  const handleOpenNavMenu = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorElNav(event.currentTarget);
  };

  const handleCloseNavMenu = () => {
    setAnchorElNav(null);
  };

  const handleMenuClick = (item: string) => {
    handleCloseNavMenu();
    switch (item) {
      case 'Contact':
        history.push(Routes.WELCOME);
        break;
      case 'A propos':
        history.push(Routes.WELCOME);
        break;
      case 'ajouter':
        history.push(Routes.ADD_BOTTLE);
        break;
      case 'historique':
        history.push(Routes.HISTORY);
        break;
      default:
        handleCloseNavMenu();
        break;
    }
  };

  return (
    <>
      <IconButton
        size="large"
        aria-label="account of current user"
        aria-controls="menu-appbar"
        aria-haspopup="true"
        onClick={handleOpenNavMenu}
        color="inherit"
      >
        <MenuIcon />
      </IconButton>
      <Menu
        id="menu-appbar"
        anchorEl={anchorElNav}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left',
        }}
        keepMounted
        transformOrigin={{
          vertical: 'top',
          horizontal: 'left',
        }}
        open={Boolean(anchorElNav)}
        onClose={handleCloseNavMenu}
        sx={{
          display: { xs: 'block', md: 'none' },
        }}
      >
        {pages.map(page => (
          <MenuItem key={page} onClick={() => handleMenuClick(page)}>
            <Typography textAlign="center">{page}</Typography>
          </MenuItem>
        ))}
      </Menu>
    </>
  );
};

export default BurgerMenu;
