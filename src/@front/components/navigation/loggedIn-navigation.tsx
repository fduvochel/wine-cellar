import React from 'react';

import { Routes } from '@front/core';
import ReusableNavigation from './reusable-navigation';
import Box from '@mui/material/Box';
import ImportExcel from '../import-excel';
import UserSettings from './user-settings';

const loggedInNavigationLinks = [
  { label: 'Historique', to: Routes.HISTORY },
  { label: 'Ajouter', to: Routes.ADD_BOTTLE },
];

const LoggedInNavigation: React.FC = () => {
  return (
    <>
      <Box
        sx={{
          flexGrow: 1,
          m: 'auto',
          display: { xs: 'none', md: 'flex' },
          alignItems: 'center',
          justifyContent: 'center',
        }}
      >
        <ReusableNavigation links={loggedInNavigationLinks} />
      </Box>
      <Box sx={{ flexGrow: 0 }}>
        <ImportExcel />
        <UserSettings />
      </Box>
    </>
  );
};

export default LoggedInNavigation;
