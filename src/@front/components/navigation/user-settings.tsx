import React from 'react';
import { useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';

import Avatar from '@mui/material/Avatar';
import IconButton from '@mui/material/IconButton';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import Tooltip from '@mui/material/Tooltip';
import Typography from '@mui/material/Typography';

import { Routes } from '@front/core';
import { userSignout } from '@front/core/user/actions';
import { resetBottlesState } from '@front/core/bottle/actions';
import { resetDiningState } from '@front/core/voucher/action';
import { resetStocksState } from '@front/core/stock/action';

const settings = ['Profile', 'Account', 'Dashboard', 'Logout'];

const UserSettings: React.FC = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const [anchorElUser, setAnchorElUser] = React.useState<null | HTMLElement>(
    null,
  );

  const handleOpenUserMenu = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorElUser(event.currentTarget);
  };

  const handleCloseUserMenu = () => {
    setAnchorElUser(null);
  };

  const handleMenuClick = (item: string) => {
    handleCloseUserMenu();
    switch (item) {
      case 'Profile':
        goToProfile();
        break;
      case 'Logout':
        handleSignOut();
        break;
      default:
        handleCloseUserMenu();
        break;
    }
  };

  const handleSignOut = () => {
    localStorage.clear();
    dispatch(userSignout());
    dispatch(resetBottlesState());
    dispatch(resetDiningState());
    dispatch(resetStocksState());
    history.push(Routes.WELCOME);
  };

  const goToProfile = () => {
    history.push(Routes.ACCOUNT);
  };

  return (
    <>
      <Tooltip title="Open settings">
        <IconButton onClick={handleOpenUserMenu} sx={{ p: 0 }} size="large">
          <Avatar alt="Remy Sharp" src="/images/avatar_sample.jpg" />
        </IconButton>
      </Tooltip>
      <Menu
        sx={{ mt: '45px' }}
        id="menu-appbar"
        anchorEl={anchorElUser}
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
        keepMounted
        transformOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
        open={Boolean(anchorElUser)}
        onClose={handleCloseUserMenu}
      >
        {settings.map(setting => (
          <MenuItem key={setting} onClick={() => handleMenuClick(setting)}>
            <Typography textAlign="center">{setting}</Typography>
          </MenuItem>
        ))}
      </Menu>
    </>
  );
};

export default UserSettings;
