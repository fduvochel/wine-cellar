import React from 'react';
import { useHistory } from 'react-router-dom';
import { useSelector } from 'react-redux';

import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import Box from '@mui/material/Box';

import { IAppState, Routes } from '@front/core';
import { IUser } from '@shared';
import LoggedInNavigation from './loggedIn-navigation';
import BurgerMenu from './burger-menu';
import Button from '@mui/material/Button';
import LoggedOutNavigation from './loggedOut-navigation';

const ResponsiveAppBar: React.FC = () => {
  const history = useHistory();

  const connectedUser = useSelector<IAppState, IUser | undefined>(
    ({ user }) => user.user,
  );

  const goHome = () => {
    history.push(connectedUser ? Routes.HOME : Routes.WELCOME);
  };

  return (
    <>
      <AppBar position="fixed">
        <Toolbar disableGutters>
          <Box sx={{ display: { xs: 'flex', md: 'none' } }}>
            <BurgerMenu isAuthed={!!connectedUser} />
          </Box>
          <Button
            sx={{
              mr: { xs: 2, md: 0 },
              flexGrow: { xs: 1, md: 0 },
              display: 'flex',
              color: 'white',
            }}
            onClick={goHome}
          >
            My cellar
          </Button>
          {connectedUser ? <LoggedInNavigation /> : <LoggedOutNavigation />}
        </Toolbar>
      </AppBar>
      <Toolbar />
    </>
  );
};

export default ResponsiveAppBar;
