import * as React from 'react';
import { Link as RouterLink } from 'react-router-dom';
import Link from '@mui/material/Link';
import Box from '@mui/material/Box';

interface LinkRouterProps {
  to: string;
  children: React.ReactNode;
}

const LinkRouter: React.FC<LinkRouterProps> = ({ to, children }) => {
  return (
    <Box sx={{ typography: 'body1', margin: '0 0.5rem' }}>
      <Link sx={{ color: 'inherit' }} component={RouterLink} to={to}>
        {children}
      </Link>
    </Box>
  );
};

export default LinkRouter;
