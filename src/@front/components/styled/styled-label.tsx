import styled from 'styled-components';

export const StyledLabel = styled.label`
  color: var(--color-text-light);
  font-style: italic;
`;

export const QuantityLabel = styled.label`
  font-weight: bold;
  opacity: 60%;
  border-radius: 2em;
  position: absolute;
  top: 0.4em;
  left: 0.4em;
  text-align: center;
  padding: 0.2em;
  max-width: 2em;
`;
