import Button from '@mui/material/Button';
import styled from 'styled-components';

export const StyledButton = styled(Button)`
  border-radius: var(--radius-border);
  background-color: var(--color-background-analogous);
  color: var(--color-background-light);
`;

export const StyledButtonNoBorder = styled(StyledButton)`
  border: none;
  margin: 0.2rem;
`;

export const GradButton = styled(StyledButtonNoBorder)`
  background-image: linear-gradient(
    to right,
    var(--color-background) 0%,
    var(--color-background-dark) 51%,
    var(--color-background) 100%
  );
  transition: 0.5s;
  background-size: 200% auto;
  color: var(--color-background-light);
  box-shadow: 0 0 10px var(--color-background-dark);
  border-radius: var(--radius-border);
  display: block;
  &:hover {
    background-position: right center;
    text-decoration: none;
  }
`;

export const PrimaryGradButton = styled.button`
  border: none;
  background-image: linear-gradient(
    to right,
    var(--color-background) 0%,
    var(--color-background-dark) 51%,
    var(--color-background) 100%
  );
  transition: 0.5s;
  background-size: 200% auto;
  color: var(--color-background-light);
  box-shadow: 0 0 10px var(--color-background-dark);
  border-radius: var(--radius-border);
  padding: 2px 8px 2px 8px;
  display: block;
  &:hover {
    background-position: right center;
    text-decoration: none;
  }
`;
