import styled from 'styled-components';
import Select from '@mui/material/Select';

export const StyledSelect = styled(Select)`
  border-radius: var(--radius-border);
`;
