import styled from 'styled-components';

export const StyledTitle = styled.h1`
  font-size: 1.9em;
  text-align: center;
`;

export const StyledSubtitle = styled.h4`
  font-size: 1.1em;
  font-style: italic;
  text-align: center;
`;

export const StyledTitleWrapper = styled.section`
  padding: 1.5rem;
  margin-bottom: 2rem;
  border-radius: 3px;
  display: flex;
  flex-direction: column;
`;
