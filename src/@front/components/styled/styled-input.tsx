import styled from 'styled-components';
import TextField from '@mui/material/TextField';

export const StyledInput = styled.input`
  border-radius: var(--radius-border);
`;

export const StyledInputText = styled(TextField)`
  border: none;
  border-radius: 0.5rem;
  margin-bottom: 1em;
`;
