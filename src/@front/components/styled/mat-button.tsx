import styled from 'styled-components';
import Button from '@mui/material/Button';

export const StyledButton = styled(Button)`
  border-radius: var(--radius-border);
  background-color: var(--color-background-analogous);
  color: var(--color-background-light);
`;
