import React from 'react';
import { useSelector } from 'react-redux';

import { IAppState } from '../core';
import { Status } from '../core/_enum/Status';
import MessageCustom from './utils/message-custom';

const Toaster: React.FC = () => {
  const error = useSelector<IAppState, string | null>(
    ({ bottles }) => bottles.error,
  );

  const success = useSelector<IAppState, boolean>(
    ({ bottles }) => bottles.status === Status.COMPLETED,
  );

  if (error) {
    return <MessageCustom level="error" content={error} duration={6000} />;
  } else if (success) {
    return (
      <MessageCustom
        level="success"
        content={Status.COMPLETED}
        duration={6000}
      />
    );
  } else {
    return null;
  }
};

export default Toaster;
