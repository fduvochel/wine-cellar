import React from 'react';

import Typography from '@mui/material/Typography';
import Avatar from '@mui/material/Avatar';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';

const SignupTitle: React.FC = () => {
  return (
    <div>
      <Avatar>
        <LockOutlinedIcon />
      </Avatar>
      <Typography component="h3" variant="h5">
        Créer un compte
      </Typography>
    </div>
  );
};

export default SignupTitle;
