import React, { useState } from 'react';

import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';

import LoginForm from './login-form';
import SignUpForm from './signup-form';
import LoginTitle from './login-title';
import SignupTitle from './signup-title';

const AuthModal: React.FC = () => {
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const [hasAccount, setHasAccount] = useState(true);

  const handleClose = () => {
    setHasAccount(true);
    setOpen(false);
  };

  const handleCancel = () => {
    setOpen(false);
  };

  const switchForm = () => {
    setHasAccount(prev => !prev);
  };

  return (
    <>
      <Button color="secondary" onClick={handleClickOpen}>
        Connexion
      </Button>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">
          {hasAccount ? <LoginTitle /> : <SignupTitle />}
        </DialogTitle>
        <DialogContent>
          {hasAccount ? (
            <LoginForm onChange={switchForm} onFinish={handleClose} />
          ) : (
            <SignUpForm onChange={switchForm} onFinish={handleClose} />
          )}
        </DialogContent>
        <DialogActions>
          <Button onClick={handleCancel} color="primary">
            annuler
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
};

export default AuthModal;
