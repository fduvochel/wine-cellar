import React from 'react';
import { Redirect, Route } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';

import { IAppState, Routes } from '../core';
import { IUser } from '@shared';
import { fetchUser } from '../core/user/actions';

interface RouterWrapperProps {
  path: string;
  component: React.FC | any;
  exact?: boolean;
}

const RouteWrapper: React.FC<RouterWrapperProps> = ({ path, component }) => {
  const dispatch = useDispatch();
  const user = useSelector<IAppState, IUser | undefined>(
    ({ user }) => user.user,
  );
  let token = localStorage.getItem('TOKEN');

  if (!user && token) {
    dispatch(fetchUser());
    token = localStorage.getItem('TOKEN');
  }

  if (!token) {
    return <Redirect to={Routes.WELCOME} />;
  } else {
    return <Route path={path} component={component} />;
  }
};

export default RouteWrapper;
