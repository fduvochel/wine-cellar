/**
 * @jest-environment jsdom
 */

import React from 'react';
import { Provider } from 'react-redux';
import { render, screen } from '@testing-library/react';

import StockDetails from '../stock-details';
import configureStore from 'redux-mock-store';
import { stockMock } from '@front/mocks/stock-mock';

const initialState = {};
const mockStore = configureStore();
const store = mockStore(initialState);

describe('StockDetails component', () => {
  beforeEach(() => {
    render(
      <Provider store={store}>
        <StockDetails stock={stockMock} />
      </Provider>,
    );
  });

  test('loads and display all components', () => {
    expect(screen.getByRole('table')).toBeDefined();
    expect(screen.getByRole('button')).toBeDefined();
    expect(screen.getByText('Fournisseur')).toBeDefined();
    expect(screen.getByText('stock supplier')).toBeDefined();
    expect(screen.getByText('Quantité restante')).toBeDefined();
    expect(screen.getByText('2')).toBeDefined();
    expect(screen.getByText('Prix')).toBeDefined();
    expect(screen.getByText('10')).toBeDefined();
  });
});
