import { IBottleResponse } from '@shared';
import { BottleActions } from '../../core/bottle/actions';
import { bottleReducer } from '../../core/bottle/reducer';
import { bottleListMock } from '../../mocks/bottle-list-mock';

describe('BottleReducer', () => {
  it('should return the pending state', () => {
    expect(
      bottleReducer(undefined, { type: BottleActions.FETCH_PENDING }),
    ).toEqual({
      status: 'pending',
      list: {},
      error: null,
    });
  });

  it('should return state with bottle list', () => {
    const action = {
      type: BottleActions.FETCH_SUCCESS,
      payload: bottleListMock,
    };
    const bottlesRecord = bottleListMock.reduce(
      (acc: any, bottle: IBottleResponse) => ({
        ...acc,
        [bottle.id]: bottle,
      }),
      {},
    );
    expect(bottleReducer(undefined, action)).toEqual({
      status: 'completed',
      list: bottlesRecord,
      error: null,
    });
  });
});
