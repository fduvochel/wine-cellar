import React, { useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import Typography from '@mui/material/Typography';
import Grid from '@mui/material/Grid';
import Container from '@mui/material/Container';
import TextField from '@mui/material/TextField';

import { IAppState } from '../../core';
import {
  fetchBottles,
  resetSearch,
  searchBottle,
} from '../../core/bottle/actions';
import BottleList from './components/bottles-list';
import { IBottleResponse } from '@shared';
import { Status } from '../../core/_enum/Status';
import LoadingCard from './components/loading-card';
import SortBottles from '../../components/filters/sort-bottles';
import ImportExcel from '../../components/import-excel';
import DownloadBottles from './components/download-bottles';

const HomePage: React.FC = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  const allBottles = useSelector<IAppState, IBottleResponse[]>(({ bottles }) =>
    Object.values(bottles.list).filter(({ domain }) =>
      domain
        .toLowerCase()
        .includes(bottles.search ? bottles.search.toLowerCase() : ''),
    ),
  );

  useEffect(() => {
    if (allBottles.length === 0) {
      dispatch(fetchBottles());
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const isPending = useSelector<IAppState, boolean>(
    ({ bottles }) => bottles.status === Status.PENDING,
  );

  if (isPending) {
    const arr = [1, 2, 3, 4];
    return (
      <>
        <Grid container spacing={1} alignItems="flex-end">
          {arr.map(item => (
            <Grid item key={item} xs={12} sm={6} md={4}>
              <LoadingCard />
            </Grid>
          ))}
        </Grid>
      </>
    );
  }

  const handleBottleSearch = (evt: React.ChangeEvent<HTMLInputElement>) => {
    dispatch(searchBottle(evt.target.value));
  };

  const handleBottleClick = (id: string) => {
    dispatch(resetSearch());
    history.push(`/bottle/${id}`);
  };

  const title = `Mes bouteilles (${allBottles.length})`;

  return (
    <Container>
      <Grid container spacing={2}>
        <Grid item xs={12} md={6}>
          <Typography variant="h4" component="div" gutterBottom>
            {title}
          </Typography>
        </Grid>
        <Grid item xs={12} md={6}>
          <TextField
            fullWidth
            type="search"
            placeholder="nom de domaine..."
            onInput={handleBottleSearch}
          />
        </Grid>
        <Grid item xs={12} md={8}>
          <SortBottles bottles={allBottles} />
        </Grid>
        <Grid item xs={12} md={4} justifyContent="flex-end">
          <DownloadBottles />
        </Grid>
      </Grid>
      <BottleList bottles={allBottles} handleCardClick={handleBottleClick} />
      {allBottles.length === 0 && <ImportExcel />}
    </Container>
  );
};

export default HomePage;
