/**
 * @jest-environment jsdom
 */

import React from 'react';
import { render, screen } from '@testing-library/react';
import BottleList from '../components/bottles-list';
import { bottleListMock } from '@front/mocks/bottle-list-mock';

const onClick = jest.fn();

beforeEach(() => {
  render(<BottleList bottles={bottleListMock} handleCardClick={onClick} />);
});
describe('BottleList component', () => {
  test('loads and display all components', () => {
    expect(screen.getByTestId('bottle-container')).toBeDefined();
    expect(screen.getAllByAltText('sample')).toHaveLength(2);
  });
});
