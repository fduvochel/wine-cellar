import React from 'react';

import Grid from '@mui/material/Grid';
import BottleCard from './bottle-card';

import { IBottleResponse } from '@shared';

interface BottleListProps {
  bottles: IBottleResponse[];
  handleCardClick: (id: string) => void;
}

const BottleList: React.FC<BottleListProps> = ({
  bottles,
  handleCardClick,
}) => {
  return (
    <Grid
      container
      spacing={3}
      alignItems="flex-start"
      data-testid="bottle-container"
    >
      {bottles.map(bottle => (
        <Grid item key={bottle.id} xs={6} sm={4} md={3}>
          <BottleCard bottle={bottle} onCardClick={handleCardClick} />
        </Grid>
      ))}
    </Grid>
  );
};

export default BottleList;
