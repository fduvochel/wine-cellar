import React from 'react';
import { useDispatch } from 'react-redux';

import Button from '@mui/material/Button';

import { downloadBottles } from '@front/core/bottle/actions';

const DownloadBottles: React.FC = () => {
  const dispatch = useDispatch();

  const handleDonwnloadFile = () => {
    dispatch(downloadBottles());
  };

  return (
    <Button variant="contained" onClick={handleDonwnloadFile}>
      download
    </Button>
  );
};

export default DownloadBottles;
