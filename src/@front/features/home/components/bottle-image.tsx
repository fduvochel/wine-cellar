import React from 'react';
import Image from 'next/image';
import Box from '@mui/system/Box';

interface BottleImageProps {
  src: string;
  alt: string;
}

const BottleImage: React.FC<BottleImageProps> = ({ src, alt }) => {
  return (
    <Box sx={{ width: '100%', margin: 'auto' }}>
      <Image src={src} alt={alt} />
    </Box>
  );
};

export default BottleImage;
