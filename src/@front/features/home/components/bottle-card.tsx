import React from 'react';

import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardContent from '@mui/material/CardContent';
import Avatar from '@mui/material/Avatar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import Container from '@mui/material/Container';

import { IBottleResponse } from '@shared';
import BottleImage from './bottle-image';
import sampleImage from '@front/images/label-300x225.png';

interface BottleCardProps {
  bottle: IBottleResponse;
  onCardClick: (id: string) => void;
}

const BottleCard: React.FC<BottleCardProps> = ({ bottle, onCardClick }) => {
  const appellation = bottle.appellation ? bottle.appellation : ' - ';
  const color = bottle.color ? bottle.color : ' - ';
  const label = bottle.label ? bottle.label : ' - ';

  const handleClick = () => {
    onCardClick(bottle.id);
  };

  return (
    <Card sx={{ maxWidth: 250 }}>
      <CardHeader
        avatar={<Avatar aria-label="quantity">{bottle.quantityInStock}</Avatar>}
        title={bottle.domain}
        subheader={bottle.vintage}
      />
      <Container sx={{ display: 'flex' }}>
        <BottleImage src={sampleImage} alt={'sample'} />
      </Container>
      <CardContent>
        <Typography paragraph color="text.secondary">
          {appellation}
        </Typography>
        <Typography paragraph>{color}</Typography>
        <Typography paragraph>{label}</Typography>
        <Button variant="contained" fullWidth onClick={handleClick}>
          voir
        </Button>
      </CardContent>
    </Card>
  );
};

export default BottleCard;
