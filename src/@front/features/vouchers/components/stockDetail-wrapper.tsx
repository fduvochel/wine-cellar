import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { IStock } from '@shared';
import StockDetails from '../../../components/stock-details';
import { IAppState } from '../../../core';
import { fetchStockById } from '../../../core/stock/action';

interface StockDetailWrapperProps {
  stockId: string;
}

const StockDetailWrapper: React.FC<StockDetailWrapperProps> = ({ stockId }) => {
  const stock = useSelector<IAppState, IStock>(
    ({ stocks }) => stocks.list[stockId],
  );

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchStockById(stockId));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [stockId]);

  if (!stock) {
    return null;
  }

  return <StockDetails stock={stock} />;
};

export default StockDetailWrapper;
