import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { format } from 'date-fns';

import CircularProgress from '@mui/material/CircularProgress';
import TableContainer from '@mui/material/TableContainer';
import Table from '@mui/material/Table';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import TableCell from '@mui/material/TableCell';
import TableBody from '@mui/material/TableBody';
import Paper from '@mui/material/Paper';

import { IVoucherResponse } from '@shared';
import { IAppState } from '../../core';
import { fetchVouchers } from '../../core/voucher/action';
import Button from '@mui/material/Button';
import StockDetailWrapper from './components/stockDetail-wrapper';

const VoucherListPage: React.FC = () => {
  const voucherList = useSelector<IAppState, IVoucherResponse[]>(
    ({ vouchers }) => Object.values(vouchers.list),
  );
  const dispatch = useDispatch();

  const [stockId, setStockId] = useState<string>();

  const displayVoucherDate = (voucher: IVoucherResponse) => {
    try {
      return voucher.createdDateTime
        ? format(voucher.createdDateTime, 'dd/MM/yyyy')
        : 'no date';
    } catch {
      return 'bad format';
    }
  };

  useEffect(() => {
    dispatch(fetchVouchers());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleClick = (id: string) => {
    setStockId(id);
  };

  if (!voucherList) {
    return <CircularProgress />;
  }
  return (
    <>
      {stockId && <StockDetailWrapper stockId={stockId} />}
      <TableContainer component={Paper}>
        <Table aria-label="voucher table">
          <TableHead>
            <TableRow>
              <TableCell>Date</TableCell>
              <TableCell align="right">Domaine</TableCell>
              <TableCell align="right">Qté consommée</TableCell>
              <TableCell align="right">Destinataire</TableCell>
              <TableCell align="right">voir le stock</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {voucherList.map(voucher => (
              <TableRow
                key={voucher.id}
                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
              >
                <TableCell component="th" scope="row">
                  {displayVoucherDate(voucher)}
                </TableCell>
                <TableCell align="right">{voucher.bottleDomain}</TableCell>
                <TableCell align="right">{voucher.quantity}</TableCell>
                <TableCell align="right">{voucher.recipient}</TableCell>
                <TableCell>
                  <Button onClick={() => handleClick(voucher.stockId)}>
                    voir
                  </Button>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </>
  );
};

export default VoucherListPage;
