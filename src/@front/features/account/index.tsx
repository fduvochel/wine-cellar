import { IUser } from '@shared';
import CircularProgress from '@mui/material/CircularProgress';
import React from 'react';
import { useSelector } from 'react-redux';
import { StyledTitleWrapper } from '../../components/styled/styled-title';

import { IAppState } from '../../core';
import UserInfo from './components/user-info';

const AccountPage: React.FC = () => {
  const user = useSelector<IAppState, IUser | undefined>(
    ({ user }) => user.user,
  );

  return (
    <>
      {!user && <CircularProgress />}
      {user && (
        <StyledTitleWrapper>
          <UserInfo user={user} />
        </StyledTitleWrapper>
      )}
    </>
  );
};

export default AccountPage;
