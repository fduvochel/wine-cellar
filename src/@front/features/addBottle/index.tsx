import React from 'react';

import Typography from '@mui/material/Typography';

import BottleRegister from './components/bottle-register';

const AddBottlePage: React.FC = () => {
  return (
    <>
      <Typography variant="h3" component="div" align="center" gutterBottom>
        Ajout de stock
      </Typography>
      <BottleRegister />
    </>
  );
};

export default AddBottlePage;
