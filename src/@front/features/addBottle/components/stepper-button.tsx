import React from 'react';

import Button from '@mui/material/Button';
import Stack from '@mui/material/Stack';

export interface StepperButtonProps {
  displayPrevious: boolean;
  displayNext: boolean;
  displaySubmit: boolean;
  canNext?: boolean;
  onPrevious(): void;
  onSubmit(): void;
}

const StepperButton: React.FC<StepperButtonProps> = ({
  displayNext,
  displayPrevious,
  displaySubmit,
  canNext,
  onPrevious,
  onSubmit,
}) => {
  return (
    <Stack direction="row" spacing={2} sx={{ marginTop: '1.5rem' }}>
      {displayPrevious && (
        <Button
          sx={{ mr: 1 }}
          variant="contained"
          color="secondary"
          onClick={() => onPrevious()}
        >
          Précédent
        </Button>
      )}
      {displayNext && (
        <Button disabled={!canNext} variant="contained" type="submit">
          Suivant
        </Button>
      )}
      {displaySubmit && (
        <Button variant="contained" onClick={() => onSubmit()}>
          Confirmer
        </Button>
      )}
    </Stack>
  );
};

export default StepperButton;
