import React from 'react';
import { useForm } from 'react-hook-form';

import { IBottleRequest } from '@shared';
import StepperButton, { StepperButtonProps } from './stepper-button';
import TextField from '@mui/material/TextField';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import InputLabel from '@mui/material/InputLabel';
import RangeSlider from '../../../components/range-slider';

interface CustomizedFormProps {
  onSubmit: (bottle: IBottleRequest) => void;
  stepperButtonsProps: StepperButtonProps;
  bottle?: Partial<IBottleRequest>;
}

const currentYear = new Date().getFullYear();

const getNextYears = (): number[] => {
  const nextYears = [];
  let i: number;
  for (i = currentYear; i < currentYear + 50; i++) {
    nextYears.push(i);
  }
  return nextYears;
};

const AddBottleForm: React.FC<CustomizedFormProps> = ({
  onSubmit,
  stepperButtonsProps,
  bottle,
}) => {
  const { handleSubmit, register, setValue, watch } = useForm<IBottleRequest>({
    defaultValues: { readyYear: currentYear, ...bottle },
  });

  const readyYear = watch('readyYear');

  const handleChangeApogee = (range: any) => {
    setValue('plateauStartYear', range[0]);
    setValue('plateauEndYear', range[1]);
  };

  const onRegister = (bottle: IBottleRequest) => {
    onSubmit(bottle);
  };

  return (
    <form onSubmit={handleSubmit(onRegister)}>
      <TextField
        variant="outlined"
        margin="normal"
        required
        fullWidth
        id="domaine"
        label="Nom de domaine"
        autoComplete="domain"
        autoFocus
        {...register('domain')}
      />
      <TextField
        variant="outlined"
        margin="normal"
        type="number"
        defaultValue={1990}
        required
        fullWidth
        id="vintage"
        label="Millésime"
        autoComplete="vintage"
        autoFocus
        {...register('vintage')}
      />
      <TextField
        variant="outlined"
        margin="normal"
        required
        fullWidth
        id="color"
        label="Couleur"
        autoComplete="color"
        autoFocus
        {...register('color')}
      />
      <TextField
        variant="outlined"
        margin="normal"
        fullWidth
        id="region"
        label="Région"
        autoComplete="region"
        autoFocus
        {...register('region')}
      />
      <TextField
        variant="outlined"
        margin="normal"
        fullWidth
        id="appellation"
        label="Appellation"
        autoComplete="appellation"
        autoFocus
        {...register('appellation')}
      />
      <TextField
        variant="outlined"
        margin="normal"
        fullWidth
        id="label"
        label="Cuvée"
        autoComplete="label"
        autoFocus
        {...register('label')}
      />
      <InputLabel id="ready-year-label">A boire à partir de</InputLabel>
      <Select
        id="readyYear"
        label="A boire à partir de"
        defaultValue={currentYear}
        labelId="ready-year-label"
        {...register('readyYear')}
      >
        {getNextYears().map(year => (
          <MenuItem key={year} value={year}>
            {year}
          </MenuItem>
        ))}
      </Select>
      <RangeSlider
        min={readyYear}
        max={currentYear + 50}
        onChange={handleChangeApogee}
      />
      <StepperButton {...stepperButtonsProps} canNext={true} />
    </form>
  );
};

export default AddBottleForm;
