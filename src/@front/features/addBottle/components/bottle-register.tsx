import React, { useMemo, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import CircularProgress from '@mui/material/CircularProgress';
import Stepper from '@mui/material/Stepper';
import Step from '@mui/material/Step';
import StepLabel from '@mui/material/StepLabel';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';

import { IAppState } from '../../../core';
import { postBottle } from '../../../core/bottle/actions';
import { IBottleRequest, IStock } from '@shared';
import StepperButton from './stepper-button';
import { Status } from '../../../core/_enum/Status';
import AddBottleForm from './addBottle-form';
import AddStockForm from './addStock-form';

const steps = [
  {
    title: 'Info Bouteille',
    content: 'First-content',
  },
  {
    title: 'Info Stocks',
    content: 'Second-content',
  },
  {
    title: 'Confirm',
    content: 'Last-content',
  },
];

const BottleRegister: React.FC = () => {
  const isPending = useSelector<IAppState, boolean>(
    ({ bottles }) => bottles.status === Status.PENDING,
  );

  const dispatch = useDispatch();
  const history = useHistory();

  const handlePostBottle = (bottle: any) => {
    dispatch(postBottle(bottle));
    history.push('/');
  };

  const [bottle, setBottle] = useState<Partial<IBottleRequest>>();

  const [stock, setStock] = useState<Partial<IStock>>();

  const onSubmit = () => {
    (bottle as IBottleRequest).stock = stock as IStock;
    handlePostBottle(bottle);
  };

  const [current, setCurrent] = React.useState(0);

  const displayNext = useMemo(() => {
    return current < steps.length - 1;
    // eslint-disable-next-line
  }, [current]);

  const displayPrevious = useMemo(() => {
    return current > 0;
  }, [current]);

  const displaySubmit = useMemo(() => {
    return current === steps.length - 1;
  }, [current]);

  const handleSubmitBottle = (bottle: IBottleRequest) => {
    setCurrent(current + 1);
    setBottle(bottle);
  };

  const handleSubmitStock = (stock: IStock) => {
    setCurrent(current + 1);
    setStock(stock);
  };

  const prev = () => {
    setCurrent(current - 1);
  };

  const stepperButtonsProps = {
    displayNext,
    displayPrevious,
    displaySubmit,
    onPrevious: prev,
    onSubmit,
  };

  return (
    <>
      {isPending && <CircularProgress />}
      <Box
        sx={{
          bgcolor: 'white',
          p: 2,
          borderRadius: 2,
        }}
      >
        <Stepper activeStep={current} sx={{ marginBottom: '1.5rem' }}>
          {steps.map(step => {
            return (
              <Step key={step.title}>
                <StepLabel>{step.title}</StepLabel>
              </Step>
            );
          })}
        </Stepper>
        {current === 0 && (
          <AddBottleForm
            onSubmit={handleSubmitBottle}
            stepperButtonsProps={stepperButtonsProps}
            bottle={bottle}
          />
        )}
        {current === 1 && (
          <AddStockForm
            onSubmit={handleSubmitStock}
            stepperButtonsProps={stepperButtonsProps}
            stock={stock}
          />
        )}
        {current === 2 && (
          <Container
            sx={{
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
            }}
          >
            <Typography align="center">
              Enregistrer {stock?.quantity} bouteille(s) de {bottle?.domain}
            </Typography>
            <StepperButton {...stepperButtonsProps} />
          </Container>
        )}
      </Box>
    </>
  );
};

export default BottleRegister;
