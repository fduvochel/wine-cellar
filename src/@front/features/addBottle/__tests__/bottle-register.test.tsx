/**
 * @jest-environment jsdom
 */

import React from 'react';
import { render, screen } from '@testing-library/react';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';

import BottleRegister from '../components/bottle-register';

const initialState = {
  bottles: {
    status: 'idle',
  },
};
const mockStore = configureStore();
const store = mockStore(initialState);

beforeEach(() => {
  render(
    <Provider store={store}>
      <BottleRegister />
    </Provider>,
  );
});

describe('BottleRegister', () => {
  test('loads and display all components', () => {
    expect(screen.getByText('Info Stocks')).toBeDefined();
    expect(screen.getAllByRole('button')).toHaveLength(2);
    expect(screen.getByText('Info Bouteille')).toBeDefined();
  });
});
