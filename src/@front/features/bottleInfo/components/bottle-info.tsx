import React from 'react';
import Image from 'next/image';

import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';

import { IBottleResponse } from '@shared';
import Typography from '@mui/material/Typography';
import SampleImage from '@front/images/label-300x225.png';

interface BottleInfoProps {
  bottle: IBottleResponse;
}

const BottleInfo: React.FC<BottleInfoProps> = ({ bottle }) => {
  return (
    <>
      <Box sx={{ flexGrow: 1 }} color="primary.main" bgcolor="white">
        <Grid container spacing={2}>
          <Grid item xs={12} sm={6}>
            <Image alt={bottle.domain} src={SampleImage} />
          </Grid>
          <Grid item xs={12} sm={6}>
            <Typography variant="h2">
              {bottle.domain} {bottle.vintage}
            </Typography>
            <Typography variant="h3">
              {bottle.color && <label>{bottle.color}</label>}
              {bottle.label && <label>{bottle.label}</label>}
            </Typography>
            <Typography variant="body1">{bottle.region}</Typography>
            <Typography variant="body1">{bottle.appellation}</Typography>
          </Grid>
          <Grid item xs={12}></Grid>
          <Grid item xs={12}>
            {bottle.readyYear}
          </Grid>
          <Grid item xs={12}>
            {bottle.plateauStartYear} - {bottle.plateauEndYear}
          </Grid>
          <Grid item xs={12}>
            {bottle.expirationString} {bottle.expirationYear}
          </Grid>
          <Grid item xs={12}>
            <Typography variant="body1">En cave :</Typography>
            <Typography variant="body1">{bottle.quantityInStock}</Typography>
          </Grid>
          <Grid item xs={12}>
            <Typography variant="subtitle2">Commentaires</Typography>
            {bottle.comment}
          </Grid>
        </Grid>
      </Box>
    </>
  );
};

export default BottleInfo;
