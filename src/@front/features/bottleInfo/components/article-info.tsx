import React from 'react';

import { IBottleResponse } from '@shared';
import StockDetails from '../../../components/stock-details';
import BottleInfo from './bottle-info';

interface ArticleInfoProps {
  bottle: IBottleResponse;
}

const ArticleInfo: React.FC<ArticleInfoProps> = ({ bottle }) => {
  return (
    <>
      <BottleInfo bottle={bottle} />
      {bottle.stocks.map(stock => (
        <StockDetails stock={stock} key={stock.id} />
      ))}
    </>
  );
};

export default ArticleInfo;
