import React from 'react';
import { useForm } from 'react-hook-form';

import { IDining } from '@shared';
import TextField from '@mui/material/TextField';
import { useVoucher } from '../voucher-context';

const DiningForm: React.FC = () => {
  const {
    state: { voucher },
  } = useVoucher();
  const { dispatch } = useVoucher();
  const { register, handleSubmit } = useForm<IDining>({
    defaultValues: voucher.dining,
  });

  const onChange = (values: IDining) => {
    voucher.dining = values;
    dispatch({ type: 'update', payload: voucher });
  };

  return (
    <form onChange={handleSubmit(onChange)}>
      <TextField
        variant="outlined"
        margin="normal"
        required
        fullWidth
        id="name"
        label="Nom de l'évènement"
        autoComplete="name"
        autoFocus
        type="text"
        {...register('name')}
      />
      <TextField
        variant="outlined"
        margin="normal"
        required
        fullWidth
        id="place"
        label="Lieu de l'évènement"
        autoComplete="location"
        autoFocus
        type="text"
        {...register('location')}
      />
    </form>
  );
};

export default DiningForm;
