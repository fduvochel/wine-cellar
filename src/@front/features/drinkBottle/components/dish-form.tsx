import React from 'react';
import { useForm } from 'react-hook-form';

import FormControlLabel from '@mui/material/FormControlLabel';
import TextField from '@mui/material/TextField';
import Checkbox from '@mui/material/Checkbox';

import { IDish } from '@shared';
import { useVoucher } from '../voucher-context';

const DishForm: React.FC = () => {
  const {
    state: { voucher },
  } = useVoucher();
  const { dispatch } = useVoucher();
  const { register, handleSubmit, setValue } = useForm<IDish>({
    defaultValues: voucher.dish,
  });

  const onChange = (values: IDish) => {
    voucher.dish = values;
    dispatch({ type: 'update', payload: voucher });
  };

  const handleClickVeggie = (event: React.ChangeEvent<HTMLInputElement>) => {
    setValue('isVeggie', event.target.checked);
  };

  const handleClickMeat = (event: React.ChangeEvent<HTMLInputElement>) => {
    setValue('isMeat', event.target.checked);
  };

  const handleClickFish = (event: React.ChangeEvent<HTMLInputElement>) => {
    setValue('isFish', event.target.checked);
  };

  const handleClickSeafood = (event: React.ChangeEvent<HTMLInputElement>) => {
    setValue('isSeafood', event.target.checked);
  };

  const handleClickDessert = (event: React.ChangeEvent<HTMLInputElement>) => {
    setValue('isDesert', event.target.checked);
  };

  return (
    <form onChange={handleSubmit(onChange)}>
      <TextField
        variant="outlined"
        margin="normal"
        required
        fullWidth
        id="name"
        label="Nom du plat"
        autoComplete="name"
        autoFocus
        type="text"
        {...register('name')}
      />
      <FormControlLabel
        name="isVeggie"
        control={<Checkbox onChange={handleClickVeggie} />}
        label="Végé"
      />
      <FormControlLabel
        control={<Checkbox onChange={handleClickMeat} />}
        label="Viande"
      />
      <FormControlLabel
        control={<Checkbox onChange={handleClickFish} />}
        label="Poisson"
      />
      <FormControlLabel
        control={<Checkbox onChange={handleClickSeafood} />}
        label="Crustacées"
      />
      <FormControlLabel
        control={<Checkbox onChange={handleClickDessert} />}
        label="Dessert"
      />
    </form>
  );
};

export default DishForm;
