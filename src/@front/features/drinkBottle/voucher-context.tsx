import React, { createContext, useContext, useReducer } from 'react';

import { IVoucher } from '@shared';
import { initialVoucher } from './initial-voucher';

type VoucherAction = { type: 'update'; payload: IVoucher };
type DispatchVoucher = (action: VoucherAction) => void;
type VoucherProviderProps = { children: React.ReactNode };
type VoucherState = {
  voucher: IVoucher;
};

const VoucherStateContext = createContext<
  { state: VoucherState; dispatch: DispatchVoucher } | undefined
>(undefined);

const voucherReducer = (state: VoucherState, action: VoucherAction) => {
  switch (action.type) {
    case 'update':
      return { voucher: action.payload };

    default:
      throw new Error(`Unhandled action type: ${action.type}`);
  }
};

export const VoucherProvider = ({ children }: VoucherProviderProps) => {
  const [state, dispatch] = useReducer(voucherReducer, {
    voucher: initialVoucher,
  });
  const value = { state, dispatch };

  return (
    <VoucherStateContext.Provider value={value}>
      {children}
    </VoucherStateContext.Provider>
  );
};

export const useVoucher = () => {
  const context = useContext(VoucherStateContext);
  if (context === undefined) {
    throw new Error('useVoucher must be used within a VoucherProvider');
  }
  return context;
};
