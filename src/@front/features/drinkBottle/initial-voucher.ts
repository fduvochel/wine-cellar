import { IVoucher } from '@shared';

export const initialVoucher: IVoucher = {
  stockId: '',
  quantity: 1,
};
