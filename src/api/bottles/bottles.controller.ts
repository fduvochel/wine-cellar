import type { Response } from 'express';

import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Res,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';

import { BottleRequestDTO } from '@model/dto/bottleRequest.dto';
import { BottleResponseDTO } from '@model/dto/bottleResponse.dto';
import { BottlesService } from './bottles.service';
import { AuthGuard } from '@auth/auth.guard';
import { User } from '@auth/user.decorator';
import { UserDTO } from '@model/dto/user.dto';

@Controller('bottles')
export class BottlesController {
  constructor(private readonly bottlesService: BottlesService) {}

  path = './public/files/bottles';
  date = new Date().toDateString().replace(/\s/g, '_');

  @Get()
  @UseGuards(AuthGuard)
  getAllBottles(@User() user: UserDTO): Promise<BottleResponseDTO[]> {
    return this.bottlesService.findAll(user);
  }

  @Get(':id')
  @UseGuards(AuthGuard)
  getBottle(@Param('id') id: string): Promise<BottleResponseDTO> {
    return this.bottlesService.findOne(id);
  }

  @Post()
  @UseGuards(AuthGuard)
  addBottle(
    @Body() bottleDTO: BottleRequestDTO,
    @User() user: UserDTO,
  ): Promise<BottleResponseDTO> {
    return this.bottlesService.createBottle(bottleDTO, user);
  }

  @Put()
  @UseGuards(AuthGuard)
  updateBottle(
    @Body() bottleDTO: BottleRequestDTO,
  ): Promise<BottleResponseDTO> {
    return this.bottlesService.saveBottle(bottleDTO);
  }

  @Delete(':id')
  removeBottle(@Param('id') id: string) {
    return this.bottlesService.deleteBottle(id);
  }

  @Post('/upload/excel')
  @UseGuards(AuthGuard)
  @UseInterceptors(FileInterceptor('file'))
  uploadFile(
    @User() user: UserDTO,
    @UploadedFile() file: { buffer: Buffer },
  ): Promise<BottleResponseDTO[]> {
    return this.bottlesService.uploadFile(file.buffer, user);
  }

  @Get('/download/excel')
  @UseGuards(AuthGuard)
  async donloadFile(@User() user: UserDTO, @Res() res: Response) {
    const workbook = await this.bottlesService.allBottlesToExcelFile(user);
    return workbook.xlsx.write(res);
  }
}
