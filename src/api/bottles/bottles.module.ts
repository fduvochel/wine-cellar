import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { Bottle } from '@model/entities/bottle.entity';
import { StockModule } from 'api/stock/stock.module';
import { InventoryModule } from '@inventory/inventory.module';
import { BottlesService } from './bottles.service';
import { BottlesController } from './bottles.controller';
import { AuthModule } from '@auth/auth.module';
import { JwtModule } from '@nestjs/jwt';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { EnvironmentVariables } from '@model/enums/environment.enum';

@Module({
  imports: [
    TypeOrmModule.forFeature([Bottle]),
    StockModule,
    InventoryModule,
    AuthModule,
    JwtModule.registerAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        secret: configService.get<string>(EnvironmentVariables.SECRET),
      }),
      inject: [ConfigService],
    }),
  ],
  providers: [BottlesService],
  controllers: [BottlesController],
})
export class BottlesModule {}
