import { Test, TestingModule } from '@nestjs/testing';
import { Repository } from 'typeorm';
import { createMock } from 'ts-auto-mock';

import { BottlesService } from './bottles.service';
import { Bottle } from '@model/entities/bottle.entity';
import { StockService } from 'api/stock/stock.service';
import { InventoryService } from '@inventory/inventory.service';
import { Stock } from '@model/entities/stock.entity';
import { Inventory } from '@model/entities/inventory.entity';
import { Voucher } from '@model/entities/voucher.entity';
import { RepositoryMock } from '@model/mocks/repository-mock';
import { BottleResponseDTO } from '@model/dto/bottleResponse.dto';
import { BottleRequestDTO } from '@model/dto/bottleRequest.dto';
import { UserDTO } from '@model/dto/user.dto';

describe.skip('BottlesService', () => {
  let service: BottlesService;
  let bottleRepository: RepositoryMock<Bottle>;
  let inventoryService: InventoryService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        BottlesService,
        StockService,
        InventoryService,
        {
          provide: 'BottleRepository',
          useValue: new RepositoryMock<Bottle>(),
        },
        {
          provide: 'StockRepository',
          useValue: createMock<Repository<Stock>>(),
        },
        {
          provide: 'VoucherRepository',
          useValue: createMock<Repository<Voucher>>(),
        },
        {
          provide: 'InventoryRepository',
          useValue: createMock<Repository<Inventory>>(),
        },
      ],
    }).compile();

    service = module.get<BottlesService>(BottlesService);
    inventoryService = module.get<InventoryService>(InventoryService);
    bottleRepository = module.get('BottleRepository');
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('findOne', () => {
    it('should return a Bottle', async () => {
      const bottle = createMock<Bottle>();
      bottle.quantityInStock = 0;
      const result = BottleResponseDTO.fromEntity(bottle);
      jest.spyOn(bottleRepository, 'findOne').mockImplementation(() => bottle);
      const res = await service.findOne('test');
      expect(res).toEqual(result);
    });
  });

  describe('createBottle', () => {
    it('should return a bottle', async () => {
      const user = new UserDTO();
      user.inventoryId = '1';
      const inventory = new Inventory();
      const bottleRequest = createMock<BottleRequestDTO>();
      const bottle = createMock<Bottle>();
      const result = BottleResponseDTO.fromEntity(bottle);
      jest.spyOn(bottleRepository, 'save').mockImplementation(() => bottle);
      jest
        .spyOn(inventoryService, 'findOne')
        .mockImplementation(async () => inventory);

      expect(await service.createBottle(bottleRequest, user)).toEqual(result);
    });
  });
});
