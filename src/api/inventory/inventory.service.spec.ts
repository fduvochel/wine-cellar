import { Test, TestingModule } from '@nestjs/testing';
import { createMock, createMockList } from 'ts-auto-mock';

import { Inventory } from '@model/entities/inventory.entity';
import { InventoryService } from './inventory.service';
import { RepositoryMock } from '@model/mocks/repository-mock';

describe.skip('InventoryService', () => {
  let service: InventoryService;
  let inventoryRepository: RepositoryMock<Inventory>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        InventoryService,
        {
          provide: 'InventoryRepository',
          useValue: new RepositoryMock<Inventory>(),
        },
      ],
    }).compile();

    service = module.get<InventoryService>(InventoryService);
    inventoryRepository = module.get('InventoryRepository');
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('findAll', () => {
    it('should return an array of Inventory', async () => {
      const result = createMockList<Inventory>(1);
      jest.spyOn(inventoryRepository, 'find').mockImplementation(() => result);
      expect(await service.findAll()).toBe(result);
    });
  });

  describe('findOne', () => {
    it('should return an Inventory', async () => {
      const result = createMock<Inventory>();
      jest
        .spyOn(inventoryRepository, 'findOne')
        .mockImplementation(() => result);
      expect(await service.findOne('test')).toBe(result);
    });
  });

  describe('saveInventory', () => {
    it('should return an Inventory', async () => {
      const result = createMock<Inventory>();
      jest.spyOn(inventoryRepository, 'save').mockImplementation(() => result);
      expect(await service.saveInventory(new Inventory())).toBe(result);
    });
  });

  describe('findLastInventory', () => {
    it('should return an Inventory', async () => {
      const inventories = createMockList<Inventory>(1);
      jest
        .spyOn(inventoryRepository, 'find')
        .mockImplementation(() => inventories);
      expect(await service.findLastInventory()).toBe(inventories[0]);
    });
  });

  describe('findLastInventory_withEmptyInventories', () => {
    it('should return an Inventory', async () => {
      const result = createMock<Inventory>();
      jest.spyOn(inventoryRepository, 'find').mockImplementation(() => null);
      jest.spyOn(inventoryRepository, 'save').mockImplementation(() => result);
      expect(await service.findLastInventory()).toEqual(result);
    });
  });
});
