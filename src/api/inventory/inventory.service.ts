import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { Inventory } from '@model/entities/inventory.entity';

@Injectable()
export class InventoryService {
  constructor(
    @InjectRepository(Inventory)
    private readonly inventoryRepository: Repository<Inventory>,
  ) {}

  public async findAll(): Promise<Inventory[]> {
    return this.inventoryRepository.find();
  }

  public async findOne(id: string): Promise<Inventory> {
    return this.inventoryRepository.findOne(id);
  }

  public async saveInventory(inventory: Inventory): Promise<Inventory> {
    return this.inventoryRepository.save(inventory);
  }

  public async findLastInventory() {
    const inventories = await this.inventoryRepository.find({
      order: { createDateTime: 'DESC' },
    });
    if (inventories) {
      return inventories[0];
    } else {
      return this.inventoryRepository.save(new Inventory());
    }
  }
}
