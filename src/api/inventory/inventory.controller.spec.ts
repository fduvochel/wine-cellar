import { Test, TestingModule } from '@nestjs/testing';
import { createMock, createMockList } from 'ts-auto-mock';

import { Inventory } from '@model/entities/inventory.entity';
import { InventoryController } from './inventory.controller';
import { InventoryService } from './inventory.service';

describe.skip('InventoryController', () => {
  let controller: InventoryController;
  let inventoryService: InventoryService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [InventoryController],
      providers: [
        { provide: InventoryService, useValue: createMock<InventoryService>() },
      ],
    }).compile();

    controller = module.get<InventoryController>(InventoryController);
    inventoryService = module.get<InventoryService>(InventoryService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('getAllInventories', () => {
    it('should return an array of Inventory', async () => {
      const result = createMockList<Inventory>(1);
      jest
        .spyOn(inventoryService, 'findAll')
        .mockImplementation(async () => result);
      expect(await controller.getAllInventories()).toBe(result);
    });
  });

  describe('getInventory', () => {
    it('should return an Inventory', async () => {
      const result = createMock<Inventory>();
      jest
        .spyOn(inventoryService, 'findOne')
        .mockImplementation(async () => result);
      expect(await controller.getInventory('test')).toBe(result);
    });
  });

  describe('addInventory', () => {
    it('should return an Inventory', async () => {
      const result = createMock<Inventory>();
      jest
        .spyOn(inventoryService, 'saveInventory')
        .mockImplementation(async () => result);
      expect(await controller.addInventory(new Inventory())).toBe(result);
    });
  });
});
