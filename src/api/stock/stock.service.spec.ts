import { Test, TestingModule } from '@nestjs/testing';
import { createMock, createMockList } from 'ts-auto-mock';
import { Repository } from 'typeorm';

import { Stock } from '@model/entities/stock.entity';
import { StockService } from './stock.service';
import { InventoryService } from '@inventory/inventory.service';
import { Inventory } from '@model/entities/inventory.entity';
import { Voucher } from '@model/entities/voucher.entity';
import { RepositoryMock } from '@model/mocks/repository-mock';
import { StockDTO } from '@model/dto/stock.dto';
import { Bottle } from '@model/entities/bottle.entity';

describe.skip('StockService', () => {
  let service: StockService;
  let stockRepository: RepositoryMock<Stock>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        StockService,
        InventoryService,
        {
          provide: 'StockRepository',
          useValue: new RepositoryMock<Stock>(),
        },
        {
          provide: 'VoucherRepository',
          useValue: createMock<Repository<Voucher>>(),
        },
        {
          provide: 'InventoryRepository',
          useValue: createMock<Repository<Inventory>>(),
        },
      ],
    }).compile();

    service = module.get<StockService>(StockService);
    stockRepository = module.get('StockRepository');
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('findAll', () => {
    it('should return an array of StockDTO', async () => {
      const stocks = createMockList<Stock>(1);
      const result = stocks.map(stock => StockDTO.fromEntity(stock));
      jest.spyOn(stockRepository, 'find').mockImplementation(() => stocks);
      expect(await service.findAll()).toEqual(result);
    });
  });

  describe('findOne', () => {
    it('should return a StockDTO', async () => {
      const stock = createMock<Stock>();
      const result = StockDTO.fromEntity(stock);
      result.bottleId = '';
      jest.spyOn(stockRepository, 'findOne').mockImplementation(() => stock);
      expect(await service.findOne('test')).toEqual(result);
    });
  });

  describe('saveStock', () => {
    it('should return a StockDTO', async () => {
      const stock = createMock<Stock>();
      jest.spyOn(stockRepository, 'save').mockImplementation(() => stock);
      expect(await service.saveStock(new StockDTO())).toEqual(
        StockDTO.fromEntity(stock),
      );
    });
  });

  describe('findByBottle', () => {
    it('should return an array of Stock', async () => {
      const stocks = createMockList<Stock>(1);
      jest.spyOn(stockRepository, 'find').mockImplementation(() => stocks);
      expect(await service.findByBottle(new Bottle())).toEqual(stocks);
    });
  });

  describe('findByBottleId', () => {
    it('should return an array of StockDTO', async () => {
      const stocks = createMockList<Stock>(1);
      jest.spyOn(stockRepository, 'find').mockImplementation(() => stocks);
      expect(await service.findStocksByBottleId('test')).toEqual(
        stocks.map(stock => StockDTO.fromEntity(stock)),
      );
    });
  });
});
