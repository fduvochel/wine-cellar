import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { Stock } from '@model/entities/stock.entity';
import { Bottle } from '@model/entities/bottle.entity';
import { StockDTO } from '@model/dto/stock.dto';

@Injectable()
export class StockService {
  constructor(
    @InjectRepository(Stock)
    private readonly stockRepository: Repository<Stock>,
  ) {}

  public async findAll(): Promise<StockDTO[]> {
    const stocks = await this.stockRepository.find();
    return stocks.map(stock => StockDTO.fromEntity(stock));
  }

  public async findOne(id: string): Promise<StockDTO> {
    const stock = await this.stockRepository.findOne(id);
    const bottle = await stock.bottle;
    const stockDTO = StockDTO.fromEntity(stock);
    stockDTO.bottleId = bottle.id;
    return stockDTO;
  }

  public async saveStock(stockDTO: StockDTO): Promise<StockDTO> {
    const stock = await this.stockRepository.save(
      StockDTO.toEntityStock(stockDTO),
    );
    return StockDTO.fromEntity(stock);
  }

  public async deleteStock(id: string) {
    return this.stockRepository.delete(id);
  }

  public async findByBottle(bottleInStock: Bottle): Promise<Stock[]> {
    return this.stockRepository.find({ where: { bottle: bottleInStock } });
  }

  public async findStocksByBottleId(bottleId: string): Promise<StockDTO[]> {
    const stocks = await this.stockRepository.find({
      where: { bottle: { id: bottleId } },
    });
    return stocks.map(stock => StockDTO.fromEntity(stock));
  }
}
