import { Controller, Get, Param, Post, Body, UseGuards } from '@nestjs/common';

import { StockDTO } from '@model/dto/stock.dto';
import { StockService } from './stock.service';
import { AuthGuard } from '@auth/auth.guard';

@Controller('stocks')
export class StockController {
  constructor(private readonly stockService: StockService) {}

  @Get()
  @UseGuards(AuthGuard)
  getAllStocks(): Promise<StockDTO[]> {
    return this.stockService.findAll();
  }

  @Get('/bottle/:bottleId')
  @UseGuards(AuthGuard)
  getStockForBottle(@Param('bottleId') id: string): Promise<StockDTO[]> {
    return this.stockService.findStocksByBottleId(id);
  }

  @Get(':stockId')
  @UseGuards(AuthGuard)
  getStockByID(@Param('stockId') id: string): Promise<StockDTO> {
    return this.stockService.findOne(id);
  }

  @Post('/update')
  @UseGuards(AuthGuard)
  updateStock(@Body() stockDTO: StockDTO): Promise<StockDTO> {
    return this.stockService.saveStock(stockDTO);
  }
}
