import { Test, TestingModule } from '@nestjs/testing';
import { createMock, createMockList } from 'ts-auto-mock';

import { BottlesService } from '@bottles/bottles.service';
import { StockController } from './stock.controller';
import { StockService } from './stock.service';
import { StockDTO } from '@model/dto/stock.dto';
import { JwtService } from '@nestjs/jwt';

describe.skip('StockController', () => {
  let controller: StockController;
  let stockService: StockService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [StockController],
      providers: [
        { provide: BottlesService, useValue: createMock<BottlesService>() },
        { provide: StockService, useValue: createMock<StockService>() },
        { provide: JwtService, useValue: createMock<JwtService>() },
      ],
    }).compile();

    controller = module.get<StockController>(StockController);
    stockService = module.get<StockService>(StockService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('findAllStocks', () => {
    it('should return an array of StockDTO', async () => {
      const result = createMockList<StockDTO>(1);
      jest
        .spyOn(stockService, 'findAll')
        .mockImplementation(async () => result);
      expect(await controller.getAllStocks()).toBe(result);
    });
  });

  describe('getStockForBottle', () => {
    it('should return an array of StockDTO', async () => {
      const result = createMockList<StockDTO>(1);
      jest
        .spyOn(stockService, 'findStocksByBottleId')
        .mockImplementation(async () => result);
      expect(await controller.getStockForBottle('test')).toBe(result);
    });
  });

  describe('getStockByID', () => {
    it('should return a StockDTO', async () => {
      const result = createMock<StockDTO>();
      jest
        .spyOn(stockService, 'findOne')
        .mockImplementation(async () => result);
      expect(await controller.getStockByID('test')).toBe(result);
    });
  });

  describe('updateStock', () => {
    it('should return a StockDTO', async () => {
      const result = createMock<StockDTO>();
      jest
        .spyOn(stockService, 'saveStock')
        .mockImplementation(async () => result);
      expect(await controller.updateStock(new StockDTO())).toBe(result);
    });
  });
});
