import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { JwtModule } from '@nestjs/jwt';
import { ConfigModule, ConfigService } from '@nestjs/config';

import { Stock } from '@model/entities/stock.entity';
import { StockService } from './stock.service';
import { StockController } from './stock.controller';
import { Voucher } from '@model/entities/voucher.entity';
import { AuthModule } from '@auth/auth.module';
import { EnvironmentVariables } from '@model/enums/environment.enum';

@Module({
  imports: [
    TypeOrmModule.forFeature([Stock, Voucher]),
    AuthModule,
    JwtModule.registerAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        secret: configService.get<string>(EnvironmentVariables.SECRET),
      }),
      inject: [ConfigService],
    }),
  ],
  providers: [StockService],
  exports: [StockService],
  controllers: [StockController],
})
export class StockModule {}
