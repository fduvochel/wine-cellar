import { createMock } from 'ts-auto-mock';
import { Test, TestingModule } from '@nestjs/testing';

import { VoucherDTO } from '@model/dto/voucher.dto';
import { Stock } from '@model/entities/stock.entity';
import { Voucher } from '@model/entities/voucher.entity';
import { RepositoryMock } from '@model/mocks/repository-mock';
import { VoucherService } from './voucher.service';

describe.skip('VoucherService', () => {
  let service: VoucherService;
  let voucherRepository: RepositoryMock<Voucher>;
  let stockRepository: RepositoryMock<Stock>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        VoucherService,
        {
          provide: 'VoucherRepository',
          useValue: new RepositoryMock<Voucher>(),
        },
        { provide: 'StockRepository', useValue: new RepositoryMock<Stock>() },
      ],
    }).compile();

    service = module.get<VoucherService>(VoucherService);
    voucherRepository = module.get('VoucherRepository');
    stockRepository = module.get('StockRepository');
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('createVoucher', () => {
    it('should return a VoucherDTO', async () => {
      const voucher = createMock<Voucher>();
      const stock = createMock<Stock>();
      jest.spyOn(stockRepository, 'findOne').mockImplementation(() => stock);
      jest.spyOn(stockRepository, 'save').mockImplementation(() => stock);
      jest.spyOn(voucherRepository, 'save').mockImplementation(() => voucher);
      expect(await service.createVoucher(new VoucherDTO())).toEqual(
        VoucherDTO.fromEntity(voucher),
      );
    });
  });
});
