import { Body, Controller, Get, Post, UseGuards } from '@nestjs/common';

import { VoucherDTO } from '@model/dto/voucher.dto';
import { VoucherResponseDTO } from '@model/dto/voucherResponse.dto';
import { VoucherService } from './voucher.service';
import { AuthGuard } from '@auth/auth.guard';

@Controller('/voucher')
export class VoucherController {
  constructor(private readonly dinngService: VoucherService) {}

  @Post()
  @UseGuards(AuthGuard)
  public addVoucher(@Body() dto: VoucherDTO): Promise<VoucherDTO> {
    return this.dinngService.createVoucher(dto);
  }

  @Get()
  @UseGuards(AuthGuard)
  public getAllVouchers(): Promise<VoucherResponseDTO[]> {
    return this.dinngService.findAll();
  }
}
