import { Test, TestingModule } from '@nestjs/testing';
import bcrypt from 'bcrypt';
import { Repository } from 'typeorm';
import { createMock } from 'ts-auto-mock';
import { ForbiddenException, NotFoundException } from '@nestjs/common';

import { RepositoryMock } from '@model/mocks/repository-mock';
import { User } from '@model/entities/user.entity';
import { UsersService } from './users.service';
import { UserDTO } from '@model/dto/user.dto';
import { UserExceptions } from '@model/enums/userExceptions.enum';
import { CredentialsDTO } from '@model/dto/credentials.dto';
import { Credentials } from '@model/entities/credentials.entity';
import { UserCreateDTO } from '@model/dto/userCreate.dto';
import { InventoryService } from '@inventory/inventory.service';
import { Inventory } from '@model/entities/inventory.entity';

describe.skip('UsersService', () => {
  let service: UsersService;
  let userRepository: RepositoryMock<User>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        UsersService,
        InventoryService,
        { provide: 'UserRepository', useValue: new RepositoryMock<User>() },
        {
          provide: 'InventoryRepository',
          useValue: createMock<Repository<Inventory>>(),
        },
      ],
    }).compile();

    service = module.get<UsersService>(UsersService);
    userRepository = module.get('UserRepository');
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('findUserByEmail', () => {
    describe('when user exists', () => {
      it('should return a userDTO', async () => {
        const user = createMock<User>();
        jest.spyOn(userRepository, 'findOne').mockImplementation(() => user);
        expect(await service.findUserByEmail('test')).toEqual(
          UserDTO.fromEntity(user),
        );
      });
    });

    describe('when user does not exist', () => {
      it('should throw an exception', async () => {
        const user = undefined;
        jest.spyOn(userRepository, 'findOne').mockImplementation(() => user);
        try {
          await service.findUserByEmail('test');
        } catch (error) {
          expect(error).toEqual(
            new NotFoundException(UserExceptions.NOT_EXISTS),
          );
        }
      });
    });
  });

  describe('findUserByCredentials', () => {
    describe('when user exists', () => {
      it('should return a UserDTO', async () => {
        const credentials = createMock<CredentialsDTO>();
        const user = createMock<User>();
        jest.spyOn(userRepository, 'findOne').mockImplementation(() => user);
        jest.spyOn(user, 'checkPassword').mockImplementation(async () => true);
        expect(await service.findUserByCredentials(credentials)).toEqual(
          UserDTO.fromEntity(user),
        );
      });
    });

    describe('when user does not exists', () => {
      it('should return a UserDTO', async () => {
        const credentials = createMock<CredentialsDTO>();
        const user = undefined;
        jest.spyOn(userRepository, 'findOne').mockImplementation(() => user);
        try {
          await service.findUserByCredentials(credentials);
        } catch (error) {
          expect(error).toEqual(
            new NotFoundException(UserExceptions.NOT_EXISTS),
          );
        }
      });
    });

    describe('when password is wrong', () => {
      it('should throw an exception', async () => {
        const credentialsDTO = createMock<CredentialsDTO>();
        const user = new User();
        const passwordHash = await bcrypt.hash('pwd', 10);
        const credentials = new Credentials('test', passwordHash);
        user.credentials = credentials;
        jest.spyOn(userRepository, 'findOne').mockImplementation(() => user);
        try {
          await service.findUserByCredentials(credentialsDTO);
        } catch (error) {
          expect(error).toEqual(
            new ForbiddenException(UserExceptions.INVALID_PASSWORD),
          );
        }
      });
    });

    describe('when check password with correct password', () => {
      it('should return true', async () => {
        const passwordHash = await bcrypt.hash('pwd', 10);
        const credentials = new Credentials('test', passwordHash);
        const user = new User();
        user.credentials = credentials;
        expect(await user.checkPassword('pwd')).toBeTruthy();
      });
    });
  });

  describe('createUser', () => {
    it('should return a userDTO', async () => {
      const userDTO = createMock<UserCreateDTO>();
      userDTO.password = 'pwd';
      const user = createMock<User>();
      jest.spyOn(userRepository, 'save').mockImplementation(() => user);
      expect(await service.createUser(userDTO)).toEqual(
        UserDTO.fromEntity(user),
      );
    });
  });
});
