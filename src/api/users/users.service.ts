import {
  ConflictException,
  ForbiddenException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import bcrypt from 'bcrypt';

import { User } from '@model/entities/user.entity';
import { UserDTO } from '@model/dto/user.dto';
import { Credentials } from '@model/entities/credentials.entity';
import { CredentialsDTO } from '@model/dto/credentials.dto';
import { UserExceptions } from '@model/enums/userExceptions.enum';
import { UserCreateDTO } from '@model/dto/userCreate.dto';
import { Inventory } from '@model/entities/inventory.entity';
import { InventoryService } from '@inventory/inventory.service';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
    private readonly inventoryService: InventoryService,
  ) {}

  public async findUserByEmail(userEmail: string): Promise<UserDTO> {
    const user = await this.userRepository.findOne({ email: userEmail });
    if (!user) {
      throw new NotFoundException(UserExceptions.NOT_EXISTS);
    }
    return UserDTO.fromEntity(user);
  }

  public async findUserByCredentials(
    credentialsDTO: CredentialsDTO,
  ): Promise<UserDTO> {
    const user = await this.userRepository.findOne({
      email: credentialsDTO.login,
    });
    if (!user) {
      throw new NotFoundException(UserExceptions.NOT_EXISTS);
    }
    const match = await user.checkPassword(credentialsDTO.password);
    if (!match) {
      throw new ForbiddenException(UserExceptions.INVALID_PASSWORD);
    }
    return UserDTO.fromEntity(user);
  }

  public async createUser(userDTO: UserCreateDTO): Promise<UserDTO> {
    const checkIfExists = await this.userRepository.findOne({
      email: userDTO.email,
    });
    if (checkIfExists) {
      throw new ConflictException(UserExceptions.ALREADY_EXISTS);
    }
    const passwordHash = await bcrypt.hash(userDTO.password, 10);
    const credentials = new Credentials(userDTO.email, passwordHash);
    const inventory = await this.inventoryService.saveInventory(
      new Inventory(),
    );
    const user = await this.userRepository.save(
      Object.assign(new User(), userDTO),
    );
    user.inventory = inventory;
    user.credentials = credentials;
    await this.userRepository.save(user);
    return UserDTO.fromEntity(user);
  }
}
