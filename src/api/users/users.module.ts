import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { User } from '@model/entities/user.entity';
import { UsersService } from './users.service';
import { Credentials } from '@model/entities/credentials.entity';
import { InventoryModule } from '@inventory/inventory.module';

@Module({
  imports: [TypeOrmModule.forFeature([User, Credentials]), InventoryModule],
  providers: [UsersService],
  exports: [UsersService],
})
export class UsersModule {}
