import {
  Body,
  Controller,
  Get,
  HttpCode,
  Post,
  UseGuards,
} from '@nestjs/common';

import { UserDTO } from '@model/dto/user.dto';
import { AuthService } from './auth.service';
import { AuthGuard } from './auth.guard';
import { CredentialsDTO } from '@model/dto/credentials.dto';
import { User } from './user.decorator';
import { UserCreateDTO } from '@model/dto/userCreate.dto';
import { UserTokenDTO } from '@model/dto/userToken.dto';

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('login')
  @HttpCode(200)
  public async login(
    @Body() credentials: CredentialsDTO,
  ): Promise<UserTokenDTO> {
    return this.authService.login(credentials);
  }

  @Post('register')
  public async register(@Body() userDTO: UserCreateDTO): Promise<UserTokenDTO> {
    return this.authService.addUser(userDTO);
  }

  @Get('me')
  @UseGuards(AuthGuard)
  public getUser(@User() user: UserDTO): Promise<UserDTO> {
    return this.authService.getUserByToken(user);
  }
}
