import { Injectable } from '@nestjs/common';

import { UserDTO } from '@model/dto/user.dto';
import { UsersService } from '@users/users.service';
import { JwtService } from '@nestjs/jwt';
import { CredentialsDTO } from '@model/dto/credentials.dto';
import { UserCreateDTO } from '@model/dto/userCreate.dto';
import { UserTokenDTO } from '@model/dto/userToken.dto';

@Injectable()
export class AuthService {
  constructor(
    private readonly userService: UsersService,
    private readonly jwtService: JwtService,
  ) {}

  private async validateUser(credentials: CredentialsDTO): Promise<UserDTO> {
    return this.userService.findUserByCredentials(credentials);
  }

  public async login(credentials: CredentialsDTO): Promise<UserTokenDTO> {
    const userDTO = await this.validateUser(credentials);
    const token = await this.jwtService.signAsync(
      {
        data: userDTO,
      },
      { expiresIn: '1d' },
    );
    return { token: token, user: userDTO };
  }

  public async addUser(user: UserCreateDTO): Promise<UserTokenDTO> {
    const savedUser = await this.userService.createUser(user);
    const token = await this.jwtService.signAsync(
      {
        data: savedUser,
      },
      { expiresIn: '1d' },
    );
    return { token: token, user: savedUser };
  }

  public async getUserByToken(userDTO: UserDTO): Promise<UserDTO> {
    return this.userService.findUserByEmail(userDTO.email);
  }
}
