import { JwtService } from '@nestjs/jwt';
import { createMock } from 'ts-auto-mock';
import { Repository } from 'typeorm';
import { Test, TestingModule } from '@nestjs/testing';

import { InventoryService } from '@inventory/inventory.service';
import { CredentialsDTO } from '@model/dto/credentials.dto';
import { UserDTO } from '@model/dto/user.dto';
import { Inventory } from '@model/entities/inventory.entity';
import { User } from '@model/entities/user.entity';
import { UsersService } from '@users/users.service';
import { AuthService } from './auth.service';

describe.skip('AuthService', () => {
  let authService: AuthService;
  let usersService: UsersService;
  let jwtService: JwtService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AuthService,
        UsersService,
        InventoryService,
        { provide: JwtService, useValue: createMock<JwtService>() },
        { provide: 'UserRepository', useValue: createMock<Repository<User>>() },
        {
          provide: 'InventoryRepository',
          useValue: createMock<Repository<Inventory>>(),
        },
      ],
    }).compile();

    authService = module.get<AuthService>(AuthService);
    usersService = module.get<UsersService>(UsersService);
    jwtService = module.get(JwtService);
  });

  it('should be defined', () => {
    expect(authService).toBeDefined();
  });

  describe('login', () => {
    it('should return a JSON object', async () => {
      const credentials = createMock<CredentialsDTO>();
      const user = createMock<UserDTO>();
      jest
        .spyOn(usersService, 'findUserByCredentials')
        .mockImplementation(async () => user);
      jest
        .spyOn(jwtService, 'signAsync')
        .mockImplementation(async () => 'jwtToken');
      expect(await authService.login(credentials)).toEqual({
        token: 'jwtToken',
        user: user,
      });
    });
  });

  describe('getUserByToken', () => {
    it('should return a userDTO', async () => {
      const result = createMock<UserDTO>();
      result.email = 'test';
      jest
        .spyOn(usersService, 'findUserByEmail')
        .mockImplementation(async () => result);
      expect(await authService.getUserByToken(result)).toBe(result);
    });
  });
});
