import { Test, TestingModule } from '@nestjs/testing';
import { createMock } from 'ts-auto-mock';
import { JwtService } from '@nestjs/jwt';

import { UsersService } from '@users/users.service';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { AuthGuard } from './auth.guard';
import { CredentialsDTO } from '@model/dto/credentials.dto';
import { UserDTO } from '@model/dto/user.dto';
import { UserCreateDTO } from '@model/dto/userCreate.dto';

describe.skip('AuthController', () => {
  let controller: AuthController;
  let authService: AuthService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AuthController],
      providers: [
        { provide: AuthService, useValue: createMock<AuthService>() },
        { provide: UsersService, useValue: createMock<UsersService>() },
        { provide: AuthGuard, useValue: createMock<AuthGuard>() },
        { provide: JwtService, useValue: createMock<JwtService>() },
      ],
    }).compile();

    controller = module.get<AuthController>(AuthController);
    authService = module.get<AuthService>(AuthService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('login', () => {
    it('should return a string', async () => {
      const user = createMock<UserDTO>();
      const token = { token: '', user: user };
      const credentials = createMock<CredentialsDTO>();
      jest.spyOn(authService, 'login').mockImplementation(async () => token);
      expect(await controller.login(credentials)).toBe(token);
    });
  });

  describe('register', () => {
    it('should return a userTokenDTO', async () => {
      const userDTO = createMock<UserDTO>();
      const userToken = { token: '', user: userDTO };
      const userCreateDTO = createMock<UserCreateDTO>();
      jest
        .spyOn(authService, 'addUser')
        .mockImplementation(async () => userToken);
      const res = await controller.register(userCreateDTO);
      expect(res.user).toEqual(userDTO);
    });
  });

  describe('me', () => {
    it('should return a userDTO', async () => {
      const userDTO = createMock<UserDTO>();
      jest
        .spyOn(authService, 'getUserByToken')
        .mockImplementation(async () => userDTO);
      expect(await controller.getUser(userDTO)).toBe(userDTO);
    });
  });
});
