import { ApiProperty } from '@nestjs/swagger';
import { IsNumber } from 'class-validator';

import { Stock } from '@model/entities/stock.entity';
import { IStock } from '@shared';

export class StockDTO implements IStock {
  @ApiProperty()
  id: string;

  @ApiProperty()
  bottleId?: string;

  @ApiProperty()
  @IsNumber()
  quantity: number;

  @ApiProperty()
  purchaseDate: Date;

  @ApiProperty()
  createDateTime: Date;

  @ApiProperty()
  position: string;

  @ApiProperty()
  owner: string;

  @ApiProperty()
  supplier: string;

  @ApiProperty()
  size: string;

  @ApiProperty()
  price: number;

  @ApiProperty()
  remainQuantity?: number;

  public static from(dto: Partial<StockDTO>) {
    return Object.assign(new StockDTO(), dto);
  }

  public static fromEntity(entity: Stock) {
    return this.from({
      id: entity.id,
      quantity: entity.quantity,
      bottleId: null,
      purchaseDate: entity.purchaseDate,
      createDateTime: entity.createDateTime,
      position: entity.position,
      owner: entity.owner,
      supplier: entity.supplier,
      size: entity.size,
      price: entity.price,
      remainQuantity: entity.remainQuantity,
    });
  }

  public static fromEntityArray(array: Stock[]) {
    return array.map(stock => this.fromEntity(stock));
  }

  public static toEntityStock(dto: StockDTO) {
    dto.bottleId = dto.bottleId ? dto.bottleId : null;
    return Object.assign(new Stock(), dto);
  }
}
