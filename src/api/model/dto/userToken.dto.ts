import { IUser, IUserToken } from '@shared';

export class UserTokenDTO implements IUserToken {
  token: string;
  user: IUser;
}
