import { IInventory } from '@shared';
import { StockDTO } from './stock.dto';

export class InventoryDTO implements IInventory {
  id: string;

  createDateTime: Date;

  stocks: StockDTO[];
}
