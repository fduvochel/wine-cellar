import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

import { Bottle } from '@model/entities/bottle.entity';
import { Photo } from '@model/entities/photo.entity';
import { StockDTO } from './stock.dto';
import { IBottleResponse } from '@shared';

export class BottleResponseDTO implements IBottleResponse {
  @ApiProperty()
  id: string;

  @ApiProperty()
  @IsString()
  domain: string;

  @ApiProperty()
  color: string;

  @ApiProperty()
  region: string;

  @ApiProperty()
  appellation: string;

  @ApiProperty()
  vintage: number;

  @ApiProperty()
  label: string;

  @ApiProperty()
  readyYear: number;

  @ApiProperty()
  plateauStartYear: number;

  @ApiProperty()
  plateauEndYear: number;

  @ApiProperty()
  expirationYear: number;

  @ApiProperty()
  expirationString?: string;

  @ApiProperty()
  comment: string;

  @ApiProperty()
  photos: Photo[];

  @ApiProperty()
  stocks: StockDTO[];

  @ApiProperty()
  quantityInStock: number;

  public static from(dto: Partial<BottleResponseDTO>) {
    return Object.assign(new BottleResponseDTO(), dto);
  }

  public static fromEntity(entity: Bottle) {
    return this.from({
      id: entity.id,
      domain: entity.domain,
      color: entity.color,
      region: entity.region,
      appellation: entity.appellation,
      vintage: entity.vintage,
      label: entity.label,
      readyYear: entity.readyYear,
      plateauStartYear: entity.plateauStartYear,
      plateauEndYear: entity.plateauEndYear,
      expirationYear: entity.expirationYear,
      expirationString: entity.expirationString,
      comment: entity.comment,
      quantityInStock: entity.quantityInStock,
      photos: entity.photos,
      stocks: entity.stocks ? StockDTO.fromEntityArray(entity.stocks) : [],
    });
  }
}
