import { ICreateUser } from '@shared';

export class UserCreateDTO implements ICreateUser {
  firstName: string;
  lastName: string;
  birthDate: Date;
  email: string;
  password: string;
  photosUrl: string[];
}
