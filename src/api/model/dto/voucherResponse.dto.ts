import { IsNumber } from 'class-validator';

import { Voucher } from '@model/entities/voucher.entity';
import { IDining, IDish, IVoucherResponse } from '@shared';

export class VoucherResponseDTO implements IVoucherResponse {
  id: string;

  @IsNumber()
  quantity: number;

  bottleId: string;

  bottleDomain: string;

  createdDateTime: Date;

  recipient: string;

  stockId: string;

  dining: IDining;

  dish: IDish;

  public static from(dto: Partial<VoucherResponseDTO>) {
    return Object.assign(new VoucherResponseDTO(), dto);
  }

  public static fromEntity(entity: Voucher) {
    return this.from({
      id: entity.id,
      quantity: entity.quantity,
      createdDateTime: entity.createdDateTime,
      recipient: entity.recipient,
      dining: entity.dining,
      dish: entity.dish,
    });
  }

  public static fromRowdata(data: any) {
    return this.from({
      id: data.voucher_id,
      quantity: data.voucher_quantity,
      bottleId: data.bottleId,
      bottleDomain: data.bottleDomain,
      createdDateTime: data.voucher_createdDateTime,
      recipient: data.voucher_recipient,
      stockId: data.voucher_stockId,
      dining: {
        id: data.dining_id,
        name: data.dining_name,
        location: data.dining_location,
      },
      dish: {
        id: data.dish_id,
        name: data.dish_name,
        isMeat: data.dish_isMeat,
        isFish: data.dish_isFish,
        isVeggie: data.dish_isVeggie,
        isSeafood: data.dish_isSeafood,
        isDesert: data.dish_isDesert,
        photos: null,
      },
    });
  }
}
