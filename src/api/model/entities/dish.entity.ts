import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

import { Voucher } from './voucher.entity';
import { Photo } from './photo.entity';

@Entity()
export class Dish {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  name: string;

  @Column({ nullable: true })
  isMeat: boolean;

  @Column({ nullable: true })
  isFish: boolean;

  @Column({ nullable: true })
  isSeafood: boolean;

  @Column({ nullable: true })
  isVeggie: boolean;

  @Column({ nullable: true })
  isDesert: boolean;

  @OneToMany(() => Voucher, voucher => voucher.dish)
  vouchers: Voucher[];

  @OneToMany(() => Photo, photo => photo.dish)
  photos: Photo[];
}
