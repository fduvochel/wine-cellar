import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

import { Dish } from './dish.entity';
import { Dining } from './dining.entity';
import { Stock } from './stock.entity';

@Entity()
export class Voucher {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ type: 'integer' })
  quantity: number;

  @CreateDateColumn({ type: 'timestamptz', default: () => 'CURRENT_TIMESTAMP' })
  createdDateTime: Date;

  @Column({ nullable: true })
  recipient: string;

  @ManyToOne(() => Stock, stock => stock.vouchers, {
    cascade: true,
    onDelete: 'CASCADE',
  })
  stock: Stock;

  @ManyToOne(() => Dish, dish => dish.vouchers, { cascade: true })
  dish: Dish;

  @ManyToOne(() => Dining, dining => dining.vouchers, { cascade: true })
  dining: Dining;
}
