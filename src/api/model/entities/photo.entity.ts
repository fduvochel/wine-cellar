import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

import { Bottle } from './bottle.entity';
import { Dish } from './dish.entity';
import { User } from './user.entity';

@Entity()
export class Photo {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  url: string;

  @ManyToOne(() => Bottle, bottle => bottle.photos)
  bottle: Bottle;

  @ManyToOne(() => User, user => user.photos)
  user: User;

  @ManyToOne(() => Dish, dish => dish.photos)
  dish: Dish;
}
