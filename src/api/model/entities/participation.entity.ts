import { Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

import { Dining } from './dining.entity';
import { Participant } from './participant.entity';

@Entity()
export class Participation {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @ManyToOne(() => Dining, dining => dining.participations, {
    onDelete: 'CASCADE',
  })
  dining: Dining;

  @ManyToOne(() => Participant, participant => participant.participations)
  participant: Participant;
}
