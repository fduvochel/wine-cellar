import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';

import { Bottle } from './bottle.entity';
import { Inventory } from './inventory.entity';
import { Voucher } from './voucher.entity';

@Entity()
export class Stock {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ type: 'integer' })
  quantity: number;

  @Column({ nullable: true })
  purchaseDate: Date;

  @CreateDateColumn({ type: 'timestamptz', default: () => 'CURRENT_TIMESTAMP' })
  createDateTime: Date;

  @Column({ nullable: true })
  position: string;

  @Column({ nullable: true })
  owner: string;

  @Column({ nullable: true })
  supplier: string;

  @Column({ nullable: true })
  size: string;

  @Column({ nullable: true, type: 'decimal' })
  price: number;

  @Column({ nullable: true })
  remainQuantity: number;

  @ManyToOne(() => Bottle, bottle => bottle.stocks, { onDelete: 'CASCADE' })
  bottle: Promise<Bottle>;

  @ManyToOne(() => Inventory, inventory => inventory.stocks)
  inventory: Inventory;

  @OneToMany(() => Voucher, voucher => voucher.stock)
  vouchers: Voucher[];
}
