import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  OneToMany,
  OneToOne,
} from 'typeorm';
import bcrypt from 'bcrypt';

import { Credentials } from './credentials.entity';
import { Inventory } from './inventory.entity';
import { Person } from './person.entity';
import { Photo } from './photo.entity';

@Entity()
export class User extends Person {
  @Column()
  email: string;

  @CreateDateColumn({ type: 'timestamptz', default: () => 'CURRENT_TIMESTAMP' })
  createDateTime: Date;

  @Column({ nullable: true })
  expirationDate: Date;

  @OneToMany(() => Photo, photo => photo.user)
  photos: Photo[];

  @OneToOne(() => Inventory, { cascade: true, eager: true })
  @JoinColumn()
  inventory: Inventory;

  @OneToOne(() => Credentials, { cascade: true, eager: true })
  @JoinColumn()
  credentials: Credentials;

  public checkPassword(password: string): Promise<boolean> {
    return bcrypt.compare(password, this.credentials.hashPassword);
  }
}
