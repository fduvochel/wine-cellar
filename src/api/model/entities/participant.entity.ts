import { Entity, OneToMany } from 'typeorm';

import { Participation } from './participation.entity';
import { Person } from './person.entity';

@Entity()
export class Participant extends Person {
  @OneToMany(() => Participation, participation => participation.participant)
  participations: Participation[];
}
