import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

import { Voucher } from './voucher.entity';
import { Participation } from './participation.entity';

@Entity()
export class Dining {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  name: string;

  @Column({ nullable: true })
  location: string;

  @OneToMany(() => Voucher, voucher => voucher.dining)
  vouchers: Voucher[];

  @OneToMany(() => Participation, participation => participation.dining)
  participations: Participation[];
}
