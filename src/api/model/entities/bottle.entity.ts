import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

import { Photo } from './photo.entity';
import { Stock } from './stock.entity';

@Entity()
export class Bottle {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  domain: string;

  @Column({ nullable: true })
  color: string;

  @Column({ nullable: true })
  region: string;

  @Column({ nullable: true })
  appellation: string;

  @Column({ nullable: true })
  vintage: number;

  @Column({ nullable: true })
  label: string;

  @Column({ nullable: true })
  readyYear: number;

  @Column({ nullable: true })
  plateauStartYear: number;

  @Column({ nullable: true })
  plateauEndYear: number;

  @Column({ nullable: true })
  expirationYear: number;

  @Column({ nullable: true })
  expirationString: string;

  @Column({ nullable: true, type: 'varchar', length: 500 })
  comment: string;

  @OneToMany(() => Photo, photo => photo.bottle, { cascade: true, eager: true })
  photos: Photo[];

  @OneToMany(() => Stock, stock => stock.bottle, { cascade: true, eager: true })
  stocks: Stock[];

  quantityInStock?: number;
}
