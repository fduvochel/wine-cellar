export class RepositoryMock<T> {
  public find(): T[] {
    return null;
  }

  public save(): T {
    return null;
  }

  public findOne(): T {
    return;
  }

  public delete() {
    return null;
  }
}
