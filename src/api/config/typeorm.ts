import { ConfigService } from '@nestjs/config';
import { ConnectionOptions } from 'typeorm';

import { Bottle } from '@model/entities/bottle.entity';
import { Dish } from '@model/entities/dish.entity';
import { Dining } from '@model/entities/dining.entity';
import { Inventory } from '@model/entities/inventory.entity';
import { Voucher } from '@model/entities/voucher.entity';
import { Participant } from '@model/entities/participant.entity';
import { Participation } from '@model/entities/participation.entity';
import { Photo } from '@model/entities/photo.entity';
import { Stock } from '@model/entities/stock.entity';
import { User } from '@model/entities/user.entity';
import { Credentials } from '@model/entities/credentials.entity';

export const typeOrmConfigFactory = (
  config: ConfigService,
): ConnectionOptions => ({
  type: 'postgres',
  host: config.get('POSTGRES_HOST'),
  port: parseInt(config.get('POSTGRES_PORT')),
  username: config.get('POSTGRES_USER'),
  password: config.get('POSTGRES_PASSWORD'),
  database: config.get('POSTGRES_DATABASE'),
  entities: [
    Photo,
    User,
    Bottle,
    Stock,
    Participant,
    Participation,
    Voucher,
    Inventory,
    Dining,
    Dish,
    Credentials,
  ],
  synchronize: true,
  ssl: config.get('NODE_ENV') === 'production',
});
