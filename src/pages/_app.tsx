import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import { Provider } from 'react-redux';
import {
  ThemeProvider,
  Theme,
  StyledEngineProvider,
} from '@mui/material/styles';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';
// import dynamic from 'next/dynamic';
import { CacheProvider } from '@emotion/react';

import Container from '@mui/material/Container';
import Typography from '@mui/material/Typography';

import { configureStore, Routes } from '@front/core';
import VoucherApi from '@front/core/_api/voucherApi';
import { StockApi } from '@front/core/_api/stockApi';
import theme from '@front/theme';
import BottleApi from '@front/core/_api/bottleApi';
import UserApi from '@front/core/_api/userApi';

import ResponsiveAppBar from '@front/components/navigation/responsive-appbar';
import RouteWrapper from '@front/components/route-wrapper';
import WelcomePage from './views/welcome';
import AppRoutes from './views/app';
import SafeHydrate from '@front/components/safe-hydrate';
import createEmotionCache from 'styles/createEmotionCache';

declare module '@mui/material/styles' {
  // eslint-disable-next-line @typescript-eslint/no-empty-interface
  interface DefaultTheme extends Theme {}
}

const bottleApi = new BottleApi();
const stockApi = new StockApi();
const voucherApi = new VoucherApi();
const userApi = new UserApi();
const currentYear = new Date().getFullYear();

const clientSideEmotionCache = createEmotionCache();

function App() {
  // const { emotionCache = clientSideEmotionCache } = props;
  return (
    <SafeHydrate>
      <LocalizationProvider dateAdapter={AdapterDateFns}>
        <Provider
          store={configureStore({ bottleApi, stockApi, voucherApi, userApi })}
        >
          <CacheProvider value={clientSideEmotionCache}>
            <StyledEngineProvider injectFirst>
              <ThemeProvider theme={theme}>
                <BrowserRouter>
                  <ResponsiveAppBar />
                  <Container maxWidth="md" sx={{ minHeight: '90vh' }}>
                    <Route
                      exact
                      path={Routes.WELCOME}
                      component={WelcomePage}
                    />
                    <RouteWrapper path={Routes.HOME} component={AppRoutes} />
                  </Container>
                </BrowserRouter>
                <Container maxWidth="md">
                  <Typography
                    variant="caption"
                    display="block"
                    align="center"
                    gutterBottom
                  >
                    Valley Design ©{currentYear} Created by CF
                  </Typography>
                </Container>
              </ThemeProvider>
            </StyledEngineProvider>
          </CacheProvider>
        </Provider>
      </LocalizationProvider>
    </SafeHydrate>
  );
}

export default App;
