import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { Routes } from '@front/core/_enum/Routes';
import AccountPage from '@front/features/account';
import AddBottlePage from '@front/features/addBottle';
import BottleInfoPage from '@front/features/bottleInfo';
import DrinkBottlePage from '@front/features/drinkBottle';
import HomePage from '@front/features/home';
import VoucherListPage from '@front/features/vouchers';

const AppRoutes: React.FC = () => {
  return (
    <Switch>
      <Route exact path={Routes.HOME} component={HomePage} />
      <Route path={Routes.ADD_BOTTLE} component={AddBottlePage} />
      <Route path={Routes.BOTTLE_INFO} component={BottleInfoPage} />
      <Route path={Routes.DRINK_BOTTLE} component={DrinkBottlePage} />
      <Route path={Routes.HISTORY} component={VoucherListPage} />
      <Route path={Routes.ACCOUNT} component={AccountPage} />
    </Switch>
  );
};

export default AppRoutes;
