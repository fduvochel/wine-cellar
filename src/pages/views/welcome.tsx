import React from 'react';
import { useSelector } from 'react-redux';
import { Redirect } from 'react-router';
import Image from 'next/image';

import Grid from '@mui/material/Grid';

import { IAppState, Routes } from '@front/core';
import { IUser } from '@shared';
import welcomeImage from '@front/images/welcome-cellar.jpg';

const WelcomePage: React.FC = () => {
  const user = useSelector<IAppState, IUser | undefined>(
    ({ user }) => user.user,
  );
  if (user) {
    return <Redirect to={Routes.HOME} />;
  }

  return (
    <Grid
      container
      direction="column"
      justifyContent="center"
      alignItems="center"
    >
      <Grid item xs={8}>
        <Image
          alt="welcome"
          src={welcomeImage}
          // priority
          // height={144}
          // width={144}
        />
      </Grid>
    </Grid>
  );
};

export default WelcomePage;
